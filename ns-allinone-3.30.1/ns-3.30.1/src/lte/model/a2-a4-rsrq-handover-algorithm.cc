/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 * Copyright (c) 2013 Budiarto Herman
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Original work authors (from lte-enb-rrc.cc):
 * - Nicola Baldo <nbaldo@cttc.es>
 * - Marco Miozzo <mmiozzo@cttc.es>
 * - Manuel Requena <manuel.requena@cttc.es>
 *
 * Converted to handover algorithm interface by:
 * - Budiarto Herman <budiarto.herman@magister.fi>
 */

#include "a2-a4-rsrq-handover-algorithm.h"
#include <ns3/log.h>
#include <ns3/uinteger.h>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("A2A4RsrqHandoverAlgorithm");

NS_OBJECT_ENSURE_REGISTERED (A2A4RsrqHandoverAlgorithm);


///////////////////////////////////////////
// Handover Management SAP forwarder
///////////////////////////////////////////


A2A4RsrqHandoverAlgorithm::A2A4RsrqHandoverAlgorithm ()
  : m_a2MeasId (0),
    m_a4MeasId (0),
    m_servingCellThreshold (30),
    m_neighbourCellOffset (1),
    m_handoverManagementSapUser (0)
{
  NS_LOG_FUNCTION (this);
  NS_LOG_INFO ("A2A4 handover algorithm is initialized.");
  m_handoverManagementSapProvider = new MemberLteHandoverManagementSapProvider<A2A4RsrqHandoverAlgorithm> (this);
}


A2A4RsrqHandoverAlgorithm::~A2A4RsrqHandoverAlgorithm ()
{
  NS_LOG_FUNCTION (this);
}


TypeId
A2A4RsrqHandoverAlgorithm::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::A2A4RsrqHandoverAlgorithm")
    .SetParent<LteHandoverAlgorithm> ()
    .SetGroupName("Lte")
    .AddConstructor<A2A4RsrqHandoverAlgorithm> ()
    .AddAttribute ("ServingCellThreshold",
                   "If the RSRQ of the serving cell is worse than this "
                   "threshold, neighbour cells are consider for handover. "
                   "Expressed in quantized range of [0..34] as per Section "
                   "9.1.7 of 3GPP TS 36.133.",
                   UintegerValue (30),
                   MakeUintegerAccessor (&A2A4RsrqHandoverAlgorithm::m_servingCellThreshold),
                   MakeUintegerChecker<uint8_t> (0, 34))
    .AddAttribute ("NeighbourCellOffset",
                   "Minimum offset between the serving and the best neighbour "
                   "cell to trigger the handover. Expressed in quantized "
                   "range of [0..34] as per Section 9.1.7 of 3GPP TS 36.133.",
                   UintegerValue (1),
                   MakeUintegerAccessor (&A2A4RsrqHandoverAlgorithm::m_neighbourCellOffset),
                   MakeUintegerChecker<uint8_t> ())
    .AddAttribute ("WindowNumber",
                   "Window size to decide the direction, 1 means only look at neighbor's current RSRQ",
                   UintegerValue (1),
                   MakeUintegerAccessor (&A2A4RsrqHandoverAlgorithm::m_window_number),
                   MakeUintegerChecker<uint8_t> ())
    .AddAttribute ("Rewards",
                   "Rewards in dB to add to the neighbor cell quaility, 0 means no reward",
                   UintegerValue (0),
                   MakeUintegerAccessor (&A2A4RsrqHandoverAlgorithm::m_rewards),
                   MakeUintegerChecker<uint8_t> ())
  ;
  return tid;
}


void
A2A4RsrqHandoverAlgorithm::SetLteHandoverManagementSapUser (LteHandoverManagementSapUser* s)
{
  NS_LOG_FUNCTION (this << s);
  m_handoverManagementSapUser = s;
}


LteHandoverManagementSapProvider*
A2A4RsrqHandoverAlgorithm::GetLteHandoverManagementSapProvider ()
{
  NS_LOG_FUNCTION (this);
  return m_handoverManagementSapProvider;
}


void
A2A4RsrqHandoverAlgorithm::DoInitialize ()
{
  NS_LOG_FUNCTION (this);

  NS_LOG_LOGIC (this << " requesting Event A2 measurements"
                     << " (threshold=" << (uint16_t) m_servingCellThreshold << ")");
  LteRrcSap::ReportConfigEutra reportConfigA2;
  reportConfigA2.eventId = LteRrcSap::ReportConfigEutra::EVENT_A2;
  reportConfigA2.threshold1.choice = LteRrcSap::ThresholdEutra::THRESHOLD_RSRQ;
  reportConfigA2.threshold1.range = m_servingCellThreshold;
  reportConfigA2.triggerQuantity = LteRrcSap::ReportConfigEutra::RSRQ;
  reportConfigA2.reportInterval = LteRrcSap::ReportConfigEutra::MS240;
  m_a2MeasId = m_handoverManagementSapUser->AddUeMeasReportConfigForHandover (reportConfigA2);
  NS_LOG_LOGIC (this << "m_a2MeasId is assigned " << (uint32_t) m_a2MeasId);

  NS_LOG_LOGIC (this << " requesting Event A4 measurements"
                     << " (threshold=0)");
  LteRrcSap::ReportConfigEutra reportConfigA4;
  reportConfigA4.eventId = LteRrcSap::ReportConfigEutra::EVENT_A4;
  reportConfigA4.threshold1.choice = LteRrcSap::ThresholdEutra::THRESHOLD_RSRQ;
  reportConfigA4.threshold1.range = 0; // intentionally very low threshold
  reportConfigA4.triggerQuantity = LteRrcSap::ReportConfigEutra::RSRQ;
  reportConfigA4.reportInterval = LteRrcSap::ReportConfigEutra::MS480;
  m_a4MeasId = m_handoverManagementSapUser->AddUeMeasReportConfigForHandover (reportConfigA4);
  NS_LOG_LOGIC (this << "m_a4MeasId is assigned " << (uint32_t) m_a4MeasId);

  LteHandoverAlgorithm::DoInitialize ();
}


void
A2A4RsrqHandoverAlgorithm::DoDispose ()
{
  NS_LOG_FUNCTION (this);
  delete m_handoverManagementSapProvider;
}


void
A2A4RsrqHandoverAlgorithm::DoReportUeMeas (uint16_t rnti,
                                           LteRrcSap::MeasResults measResults)
{

  NS_LOG_FUNCTION (this << rnti << (uint16_t) measResults.measId);

  NS_LOG_LOGIC ("A2A4 algorithm DoReportUeMeas() with rnti = " << rnti);

  if (measResults.measId == m_a2MeasId)
    {
      NS_ASSERT_MSG (measResults.rsrqResult <= m_servingCellThreshold,
                     "Invalid UE measurement report");

      NS_LOG_LOGIC ("measID == m_a2MeasId, it's an A2 event");
     
      NS_LOG_LOGIC ("A2A4 calling EvaluateHandover()");

      EvaluateHandover (rnti, measResults.rsrqResult);
    }
  else if (measResults.measId == m_a4MeasId)
    {

      NS_LOG_LOGIC ("measID == m_a4MeasId, it's an A4 event");

      if (measResults.haveMeasResultNeighCells
          && !measResults.measResultListEutra.empty ())
        {
          for (std::list <LteRrcSap::MeasResultEutra>::iterator it = measResults.measResultListEutra.begin ();
               it != measResults.measResultListEutra.end ();
               ++it)
            {
              NS_ASSERT_MSG (it->haveRsrqResult == true,
                             "RSRQ measurement is missing from cellId " << it->physCellId);
              NS_LOG_LOGIC ("Update neighbor cells whose cell id = " << it->physCellId << " with rsrq = " << (uint16_t)it->rsrqResult);
              UpdateNeighbourMeasurements (rnti, it->physCellId, it->rsrqResult);
            }
        }
      else
        {
          NS_LOG_LOGIC ("Event A4 received without measurement results from neighbouring cells");
          NS_LOG_WARN (this << " Event A4 received without measurement results from neighbouring cells");
        }
    }
  else
    {
      NS_LOG_LOGIC ("Neither a2 nor a4");

      NS_LOG_WARN ("Ignoring measId " << (uint16_t) measResults.measId);
    }

} // end of DoReportUeMeas


void
A2A4RsrqHandoverAlgorithm::EvaluateHandover (uint16_t rnti,
                                             uint8_t servingCellRsrq)
{
  NS_LOG_FUNCTION (this << rnti << (uint16_t) servingCellRsrq);

  MeasurementTable_t::iterator it1;

  NS_LOG_LOGIC ("EvaluateHandover() with rnti = " << rnti << " and serving rsrq = " << unsigned(servingCellRsrq));
  NS_LOG_LOGIC ("Neighbor offset = " << unsigned(m_neighbourCellOffset));

  NS_LOG_LOGIC ("looking for neighbors with rnti = " << rnti);
  it1 = m_neighbourCellMeasures.find (rnti);

  if (it1 == m_neighbourCellMeasures.end ())
    {
      NS_LOG_LOGIC ("Didn't find neighbor cells with rnti = " << rnti);
      NS_LOG_WARN ("Skipping handover evaluation for RNTI " << rnti << " because neighbour cells information is not found");
    }
  else
  {
    // Find the best neighbour cell (eNB)
    // it1->second.size() is the MAP size with key = cell id, value = measurements
    NS_LOG_LOGIC ("Found neighbor cells with rnti = " << rnti << ", which has " << it1->second.size () << " cells");
    uint16_t bestNeighbourCellId = 0;
    uint8_t bestNeighbourRsrq = 0;
    
    MeasurementRow_t::iterator it2;

    uint16_t bestTowardsCellId = 0;
    uint8_t bestTowardsRsrq = 0;

    // find the best rsrq from ALL the neighbor measures 
    for (it2 = it1->second.begin (); it2 != it1->second.end (); ++it2)
    {
      /*
      NS_LOG_LOGIC ("it2->second->m_rsrq = " << (uint32_t) (it2->second->m_rsrq) << std::endl
                      << "Best neighbour rsrq = " << (uint32_t) bestNeighbourRsrq << std::endl
                      << "valid = " << IsValidNeighbour (it2->first));
      */

      /*
      if ((it2->second->m_rsrq > bestNeighbourRsrq) && IsValidNeighbour (it2->first))
        {
          NS_LOG_LOGIC ("satisfied, update best neighbour cell id");
          bestNeighbourCellId = it2->first;
          bestNeighbourRsrq = it2->second->m_rsrq;
        }
      else{
        NS_LOG_LOGIC ("not satisfied, neighbour cell id = " << bestNeighbourCellId);
      }
      */

      // now the it2 is a deque of measurement pointers
      // it2->first is cell ID, it2->second is a deque 
      bool non_decreasing = true;
      uint8_t previous_rsrq = 0;

      Ptr<UeMeasure> neighbourCellMeasures;

      NS_LOG_LOGIC ("At time << " << Simulator::Now().GetSeconds() << ": list all the measures for cell id");
      for ( std::deque<Ptr<UeMeasure> >::iterator it3 = it2->second.begin(); 
              it3 != it2->second.end(); it3++ )
      {
        neighbourCellMeasures = *it3;
        //std::cout << "--------------------------------" << std::endl;
        //std::cout << "timestamp = " << neighbourCellMeasures->m_timestamp << std::endl;
        //std::cout << "rsrq = " << (uint16_t)neighbourCellMeasures->m_rsrq << std::endl;
        if (previous_rsrq <= neighbourCellMeasures->m_rsrq){
          previous_rsrq = neighbourCellMeasures->m_rsrq;
        }
        else{
          non_decreasing = false;
        }
      }

      neighbourCellMeasures = it2->second.back();
      NS_LOG_LOGIC ("latest rsrq of cell " << it2->first << " = " << (uint16_t)neighbourCellMeasures->m_rsrq);

      if ( (neighbourCellMeasures->m_rsrq > bestNeighbourRsrq) &&
              (IsValidNeighbour(it2->first)) )
      {
        bestNeighbourCellId = it2->first;
        bestNeighbourRsrq = neighbourCellMeasures->m_rsrq;
      } 
      
      if (non_decreasing){
        NS_LOG_LOGIC ("non_decreasing, record this towards neighbor");
        if ((neighbourCellMeasures->m_rsrq > bestTowardsRsrq) &&
              (IsValidNeighbour(it2->first)))
        {
          bestTowardsCellId = it2->first;
          bestTowardsRsrq = neighbourCellMeasures->m_rsrq;
        }
      }


    }

    NS_LOG_LOGIC ("Best neighbour rsrq = " << (uint32_t) bestNeighbourRsrq);

    // Trigger Handover, if needed
    if (bestNeighbourCellId > 0)
    {
      NS_LOG_LOGIC ("Best neighbor cellId " << bestNeighbourCellId);

      if ((bestNeighbourRsrq - servingCellRsrq) >= m_neighbourCellOffset)
      {
        NS_LOG_LOGIC ("Best neighbor better than current");
        NS_LOG_LOGIC ("Trigger Handover to cellId " << bestNeighbourCellId);
        NS_LOG_LOGIC ("target cell RSRQ " << (uint16_t) bestNeighbourRsrq);
        NS_LOG_LOGIC ("serving cell RSRQ " << (uint16_t) servingCellRsrq);
        NS_LOG_LOGIC ("neighbour cell offset " << (uint16_t) m_neighbourCellOffset);

        // Inform eNodeB RRC about handover
        m_handoverManagementSapUser->TriggerHandover (rnti,
                                                      bestNeighbourCellId);
      }
      else
      {
        NS_LOG_LOGIC ("Best neighbor worse than current, check early trigger condition with rewards = " << m_rewards);
        if (bestTowardsCellId > 0)
        {
          if ((bestTowardsRsrq - servingCellRsrq) >= (m_neighbourCellOffset-m_rewards))
          {
            NS_LOG_LOGIC ("Towards neighbor exists, trigger to neighbor id = " << bestTowardsCellId);
            m_handoverManagementSapUser->TriggerHandover (rnti, bestTowardsCellId);
          }
          else
          {
            NS_LOG_LOGIC ("Towards neighbor worse still bad, no handover");
          }
        }
        else
        {
          NS_LOG_LOGIC ("no valid early trigger");
        }        
      }
    }
    else{
      NS_LOG_LOGIC("best neighbour cell id == 0, which mean no neighbor exists");
    }

  } // end of else of if (it1 == m_neighbourCellMeasures.end ())

} // end of EvaluateMeasurementReport


bool
A2A4RsrqHandoverAlgorithm::IsValidNeighbour (uint16_t cellId)
{
  NS_LOG_FUNCTION (this << cellId);

  /**
   * \todo In the future, this function can be expanded to validate whether the
   *       neighbour cell is a valid target cell, e.g., taking into account the
   *       NRT in ANR and whether it is a CSG cell with closed access.
   */

  return true;
}


void
A2A4RsrqHandoverAlgorithm::UpdateNeighbourMeasurements (uint16_t rnti,
                                                        uint16_t cellId,
                                                        uint8_t rsrq)
{
  NS_LOG_FUNCTION (this << rnti << cellId << (uint16_t) rsrq);
  
  NS_LOG_LOGIC ("updating neighbor measurements, with rnti = " << rnti << ", cell id = " << cellId);

  MeasurementTable_t::iterator it1;
  it1 = m_neighbourCellMeasures.find (rnti);

  if (it1 == m_neighbourCellMeasures.end ())
    {
      NS_LOG_LOGIC ("rnti does NOT exist in the measurement map, insert a new <key, value> entry");
      
      // insert a new UE entry
      MeasurementRow_t row;
      std::pair<MeasurementTable_t::iterator, bool> ret;
      ret = m_neighbourCellMeasures.insert (std::pair<uint16_t, MeasurementRow_t> (rnti, row));
      NS_ASSERT (ret.second);
      it1 = ret.first;
    }

  NS_ASSERT (it1 != m_neighbourCellMeasures.end ());
  Ptr<UeMeasure> neighbourCellMeasures;
  std::map<uint16_t, std::deque<Ptr<UeMeasure> > >::iterator it2;
  it2 = it1->second.find (cellId);

  if (it2 != it1->second.end ())
    {
      NS_LOG_LOGIC ("Cell id " << cellId << " has a measure list already");

      /*
      neighbourCellMeasures = it2->second;
      neighbourCellMeasures->m_cellId = cellId;
      neighbourCellMeasures->m_rsrp = 0;
      neighbourCellMeasures->m_rsrq = rsrq;
      */
      neighbourCellMeasures = Create<UeMeasure> ();
      neighbourCellMeasures->m_timestamp = Simulator::Now().GetSeconds();
      neighbourCellMeasures->m_cellId = cellId;
      neighbourCellMeasures->m_rsrp = 0;
      neighbourCellMeasures->m_rsrq = rsrq;

      it2->second.push_back(neighbourCellMeasures);

      // check if the number of records exceed the window number, pop the previous ones
      if (it2->second.size() > m_window_number){
        NS_LOG_LOGIC("stored measurement > window number = " << m_window_number << ", pop the front one");
        it2->second.pop_front();
      }
    }
  else
    {
      // insert a new cell entry
      NS_LOG_LOGIC ("Cell id " << cellId << " is not in the measurement map");

      // declare an empty deque and push the measurement to it
      std::deque<Ptr<UeMeasure> > q;

      neighbourCellMeasures = Create<UeMeasure> ();
      
      neighbourCellMeasures->m_timestamp = Simulator::Now().GetSeconds();
      neighbourCellMeasures->m_cellId = cellId;
      neighbourCellMeasures->m_rsrp = 0;
      neighbourCellMeasures->m_rsrq = rsrq;

      q.push_back(neighbourCellMeasures);

      //it1->second[cellId] = neighbourCellMeasures;
      it1->second[cellId] = q;      
    }

} // end of UpdateNeighbourMeasurements


} // end of namespace ns3
