#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from xml.dom.minidom import parse
import xml.dom.minidom

"""
Output write to: flow_results.txt
format:
	exp_number X offset X total_enbs X handover X tx_packets_1 X lost_1 X tx_packets_2 X lost_2 X 

Notice that flow_1 is downlink, flow_2 is uplink
"""

if __name__ == '__main__':

	"""
	f = open("flow_results.txt", 'w')

	for exp_number in range(0, 26):
		for offset in [0, 1]:
			f.write("exp_number %d offset %d " %(exp_number, offset))

			ho_filename = "long_dataset/ho_results_speed_original_power_65_offset_%d.txt" %(offset)
			ho_file = open(ho_filename, 'r')
			line = ho_file.readlines()[exp_number].strip()
			ss = line.split(' ')
			f.write("total_enbs %s handover %s " %(ss[3], ss[5]))
			ho_file.close()

			flow_filename = "long_dataset/flow_exp_%s_power_65.000000_offset_%s.xml" %(exp_number, offset)
			DOMTree = xml.dom.minidom.parse(flow_filename)
			collection = DOMTree.documentElement
			flowStats = collection.getElementsByTagName("FlowStats")
			flows = flowStats[0].getElementsByTagName("Flow")
			f.write("tx_packets_1 %s lost_1 %s tx_packets_2 %s lost_2 %s\n"
									%(flows[0].getAttribute("txPackets"), 
										flows[0].getAttribute("lostPackets"),
										flows[1].getAttribute("txPackets"),
										flows[1].getAttribute("lostPackets")))

	f.close()
	"""

	"""
	draw line plot, with x-axis = case#, y-axis include:
		1. # enbs
		2. offset=0 #handover
		3. offset=1 #handover
		4. offset=0 #dl lost 				(flow 1 is DL)
		5. offset=1 #dl lost
		6. offset=0 #ul lost
	"""

	num_enb_list = []

	off_0_ho_list = []
	off_0_dl_list = []
	off_0_ul_list = []

	off_1_ho_list = []
	off_1_dl_list = []
	off_1_ul_list = []

	f = open("flow_results.txt", 'r')
	lines = f.readlines()
	f.close()

	for index, line in enumerate(lines):
		
		line = line.strip()
		ss = line.split(' ')

		if index % 2 == 0:
			num_enb_list.append(int(ss[5]))

		if ss[3] == '0':
			off_0_ho_list.append(int(ss[7]))
			off_0_dl_list.append(int(ss[11]))
			off_0_ul_list.append(int(ss[15]))

		elif ss[3] == '1':
			off_1_ho_list.append(int(ss[7]))
			off_1_dl_list.append(int(ss[11]))
			off_1_ul_list.append(int(ss[15]))

	print (num_enb_list)
	print (off_0_ho_list)
	print (off_1_ho_list)

	########################## drawing ############################
	fig, ax = plt.subplots()

	x_list = range(1, 27)

	ax.scatter(x_list, num_enb_list, color='black', label='#eNBs')
	ax.scatter(x_list, off_0_ho_list, color='blue', label='#ho_off_0')
	ax.scatter(x_list, off_1_ho_list, color='red', label='#ho_off_1')

	ax.set(xlabel='Case#', ylabel='Value', title='Compare ho w/ offset')
	ax.legend()
	ax.grid()

	fig.savefig("flow_results_ho.png")

	plt.clf()	

	##################################################

	fig, ax = plt.subplots()

	ax.scatter(x_list, off_0_dl_list, color='red', label='#dl_lost_off_0', s=10)
	ax.scatter(x_list,off_1_dl_list, color='blue', label='#dl_lost_off_1', s=10)

	ax.scatter(x_list, off_0_ul_list, color='green', label='#ul_lost_off_0', s=10)
	ax.scatter(x_list, off_1_ul_list, color='black', label='#ul_lost_off_1', s=10)

	ax.set(xlabel='Case#', ylabel='Value', title='Compare loss w/ offset')
	ax.legend()
	ax.grid()

	fig.savefig("flow_results_loss.png")

	plt.clf()	
	


