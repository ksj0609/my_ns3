#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np

"""
Experiment number past as argument

This is a script to automate expriment process, steps are:
	
	(1) Copy two files to NS3 directory
	(2) Generate mobility trace file and real_measurements and estimate_measurements files
	(4) Execute simulation
	(5) Generate log files and rsrp comparison 
"""

if __name__ == '__main__':

	exp_number = sys.argv[1]

	filename = "dataset/ho_results_speed_%s_power_65_offset_1.txt" %(exp_number)
	f = open(filename, 'r')

	total_counter = 0
	ho_success = 0
	ho_over = 0

	for line in f.readlines():
		strings = line.strip().split(' ')
		print (strings)
		if strings[5] != '0':
			ho_success += 1.0
		if strings[5] > '1':
			ho_over += 1.0
		total_counter += 1.0

	f.close()

	print ("success = %d, over = %d, total = %d" %(ho_success, ho_over, total_counter))
	
	success_ratio = ho_success/total_counter
	over_ratio = ho_over/total_counter

	print ("success ratio = %f, over ratio = %f" %(success_ratio, over_ratio))