/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Manuel Requena <manuel.requena@cttc.es>
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store-module.h"
#include "ns3/flow-monitor-helper.h"
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("LenaX2HandoverMeasures");

void
NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti)
{
  std::cout << context
            << " UE IMSI " << imsi
            << ": connected to CellId " << cellid
            << " with RNTI " << rnti
            << std::endl;
}

void
NotifyHandoverStartUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellid,
                       uint16_t rnti,
                       uint16_t targetCellId)
{
  std::cout << "==================================================" << std::endl
            << context << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << " UE IMSI " << imsi << std::endl
            << ": previously connected to CellId " << cellid << std::endl
            << " with RNTI " << rnti << std::endl
            << ", doing handover to CellId " << targetCellId <<std::endl
            << "==================================================" << std::endl;
}

void
NotifyHandoverEndOkUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellid,
                       uint16_t rnti)
{
  std::cout << "==================================================" << std::endl
            << context << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << " UE IMSI " << imsi << std::endl
            << ": successful handover to CellId " << cellid << std::endl
            << " with RNTI " << rnti << std::endl
            << "==================================================" << std::endl;
}

void
NotifyConnectionEstablishedEnb (std::string context,
                                uint64_t imsi,
                                uint16_t cellid,
                                uint16_t rnti)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": successful connection of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;


}

void
NotifyHandoverStartEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti,
                        uint16_t targetCellId)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": start handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << " to CellId " << targetCellId
            << std::endl;

  std::ofstream outfile;
  outfile.open("scratch/rsrp.txt", std::ofstream::out | std::ofstream::app);

  outfile << Simulator::Now ().GetSeconds () << " " << 0 << " " << 0 << " " << 0 << " " << 0 << std::endl;

  outfile.flush();
  outfile.close();
}

void
NotifyHandoverEndOkEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": completed handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;

  /*
  std::ofstream outfile;
  outfile.open("scratch/rsrp.txt", std::ofstream::out | std::ofstream::app);

  outfile << Simulator::Now ().GetSeconds () << " " << 1 << " " << 1 << " " << 1 << " " << 1 << std::endl;

  outfile.flush();
  outfile.close();
  */
}

////////////////////////// my additional trace callback ///////////////////////////

/**
   * The `ConnectionReconfiguration` trace source. Fired upon RRC connection
   * reconfiguration. Exporting IMSI, cell ID, and RNTI.
   
  TracedCallback<uint64_t, uint16_t, uint16_t> m_connectionReconfigurationTrace;
*/
void
NotifyConnectionReconfigurationEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti)
{
  std::cout << "============================================================" << std::endl
            << '\t' << context << std::endl
            << '\t' << "eNB CellId = " << cellid << std::endl
            << '\t' << "RRC reconnection configuration with UE IMSI = " << imsi << std::endl
            << '\t' << "RNTI = " << rnti << std::endl
            << "============================================================" << std::endl;
}

/**
   * The `RecvMeasurementReport` trace source. Fired when measurement report is
   * received. Exporting IMSI, cell ID, and RNTI.
   
  TracedCallback<uint64_t, uint16_t, uint16_t, LteRrcSap::MeasurementReport> m_recvMeasurementReportTrace;
*/
void
NotifyRecvMeasurementReport (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti, 
                        LteRrcSap::MeasurementReport)
{
  std::cout << "============================================================" << std::endl
            << '\t' << context << std::endl
            << '\t' << "eNB CellId = " << cellid << std::endl
            << '\t' << "Received measurement report from UE IMSI = " << imsi << std::endl
            << '\t' << "RNTI = " << rnti << std::endl
            << "============================================================" << std::endl;
}

/**
   * The `ConnectionReconfiguration` trace source. Fired upon RRC connection
   * reconfiguration. Exporting IMSI, cell ID, and RNTI.
   
  TracedCallback<uint64_t, uint16_t, uint16_t> m_connectionReconfigurationTrace;
*/
void
NotifyConnectionReconfigurationUe (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti)
{
  std::cout << "============================================================" << std::endl
            << '\t' << context << std::endl
            << '\t' << "eNB CellId = " << cellid << std::endl
            << '\t' << "UE IMSI = " << imsi << " received reconfiguration" << std::endl
            << '\t' << "RNTI = " << rnti << std::endl
            << "============================================================" << std::endl;
}


/**
   * The `ReportUeMeasurements` trace source. Contains trace information
   * regarding RSRP and RSRQ measured from a specific cell (see TS 36.214).
   * Exporting RNTI, the ID of the measured cell, RSRP (in dBm), RSRQ (in dB),
   * and whether the cell is the serving cell. Moreover it report the m_componentCarrierId.
   
  TracedCallback<uint16_t, uint16_t, double, double, bool, uint8_t> m_reportUeMeasurements;
*/
void
NotifyUdpServerRx (std::string context, Ptr<const Packet>)
{
  std::cout << "============================================================" << std::endl
            << '\t' << context << std::endl
            << "received a packet" << std::endl
            << "============================================================" << std::endl;
}


void
NotifyUeMeasurements (std::string context, 
                      uint16_t rnti,
                      uint16_t cell_id, 
                      double rsrp,
                      double rsrq, 
                      bool is_serving, 
                      uint8_t m_component_carrier_id)
{
  std::cout << "------------------------------------" << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << "reporting cell signal strength of rnti " << (uint16_t)rnti << ", cell id " << (uint16_t) cell_id << std::endl
            << "rsrp = " << rsrp << ", rsrq = " << rsrq << std::endl
            << "is_serving_cell = " << is_serving << std::endl
            << "------------------------------------" << std::endl;

  // write to file
  // assume cell id == 1 always comes first

  
  std::ofstream outfile;
  outfile.open("scratch/rsrp.txt", std::ofstream::out | std::ofstream::app);

  if (cell_id == 1){
    outfile << Simulator::Now ().GetSeconds () << " " << rsrp << " " << rsrq << " ";
  } 
  else if (cell_id == 2){
    outfile << rsrp << " " << rsrq << std::endl;
  }

  outfile.flush();
  outfile.close();
}

void
NotifySinrReport (std::string context, 
                  uint16_t cell_id, 
                  uint16_t rnti, 
                  double rsrp, 
                  double sinr, 
                  uint8_t component_carrier_id)
{
  std::cout << "------------------------------------" << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << "reporting SINR of rnti " << (uint16_t)rnti << ", cell id " << (uint16_t) cell_id << std::endl
            << "sinr = " << sinr
            << "------------------------------------" << std::endl;

  std::ofstream outfile;
  outfile.open("scratch/sinr.txt", std::ofstream::out | std::ofstream::app);

  outfile << Simulator::Now().GetSeconds() << " " << sinr << std::endl;

  outfile.flush();
  outfile.close();
}

 /**
   * The 'RadioLinkFailure' trace source. Fired when T310 timer expires.
   *
   *
   * TracedCallback<uint64_t, uint16_t, uint16_t> m_radioLinkFailureTrace;
  */
void 
RadioLinkFailure (std::string context, 
                  uint64_t imsi, 
                  uint16_t cellId, 
                  uint16_t rnti)
{
  std::cout << Simulator::Now ()
            << " IMSI " << imsi << ", RNTI " << rnti
            << ", Cell id " << cellId << ", radio link failure detected"
            << std::endl << std::endl; 
}

/**
   * The 'PhySyncDetection' trace source. Fired when UE RRC
   * receives in-sync or out-of-sync indications from UE PHY
   *
   */
void
NotifyPhySync (std::string context,
               uint64_t m_imsi,
               uint16_t m_rnti,
               uint16_t m_cellId,
               std::string message,
               uint8_t m_noOfSyncIndications)
{
  std::cout << "at time " << Simulator::Now () << std::endl
            << ": phy out of sync detected , with rnti = " << m_rnti << ", cell id = " << m_cellId << std::endl
            << "number of sync indication = " << (uint16_t) m_noOfSyncIndications << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////

/**
 * Sample simulation script for an automatic X2-based handover based on the RSRQ measures.
 * It instantiates two eNodeB, attaches one UE to the 'source' eNB.
 * The UE moves between both eNBs, it reports measures to the serving eNB and
 * the 'source' (serving) eNB triggers the handover of the UE towards
 * the 'target' eNB when it considers it is a better eNB.
 */
int
main (int argc, char *argv[])
{
  LogLevel logLevel = (LogLevel)(LOG_PREFIX_ALL | LOG_LOGIC);

  // LogComponentEnable ("LteHelper", logLevel);
  // LogComponentEnable ("EpcHelper", logLevel);
  // LogComponentEnable ("EpcEnbApplication", logLevel);
  // LogComponentEnable ("EpcX2", logLevel);
  // LogComponentEnable ("EpcSgwPgwApplication", logLevel);

  // LogComponentEnable ("LteEnbRrc", logLevel);
  // LogComponentEnable ("LteEnbNetDevice", logLevel);
  // LogComponentEnable ("LteUeRrc", logLevel);
  // LogComponentEnable ("LteUeNetDevice", logLevel);
  // LogComponentEnable ("A2A4RsrqHandoverAlgorithm", logLevel);
  // LogComponentEnable ("A3RsrpHandoverAlgorithm", logLevel);

  LogComponentEnable ("LenaX2HandoverMeasures", logLevel);

  // LogComponentEnable ("UdpClient", (LogLevel)(LOG_PREFIX_ALL | LOG_INFO));
  // LogComponentEnable ("UdpServer", (LogLevel)(LOG_PREFIX_ALL | LOG_INFO));
  // LogComponentEnable ("PacketSink", (LogLevel)(LOG_PREFIX_ALL | LOG_INFO));
  
  // LogComponentEnable ("LteUePhy", logLevel);
  // LogComponentEnable ("LteInterference", logLevel);
  // LogComponentEnable ("LteMiErrorModel", logLevel);
  LogComponentEnable ("LteSpectrumPhy", logLevel);

  // LogComponentEnable ("MultiModelSpectrumChannel", logLevel);
  // LogComponentEnable ("Config", LOG_LEVEL_ALL);
  
  uint16_t numberOfUes = 1;
  uint16_t numberOfEnbs = 2;
  uint16_t numBearersPerUe = 1;
  double distance = 1000.0; // m
  double yForUe = 100.0;   // m
  double speed = 150;       // m/s
  double simTime = (double)(numberOfEnbs + 1) * distance / speed; // 1500 m / 20 m/s = 75 secs
  double enbTxPowerDbm = 10.0;

  // change some default attributes so that they are reasonable for
  // this scenario, but do this before processing command line
  // arguments, so that the user is allowed to override these settings
  Config::SetDefault ("ns3::UdpClient::Interval", TimeValue (MilliSeconds (5)));
  Config::SetDefault ("ns3::UdpClient::MaxPackets", UintegerValue (1000000));
  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (true));

  Config::SetDefault ("ns3::LteUePhy::Qout", DoubleValue (-1));
  Config::SetDefault ("ns3::LteUeRrc::T310", TimeValue (MilliSeconds (50)));
  Config::SetDefault ("ns3::LteUeRrc::N310", UintegerValue(1));

  // Command line arguments
  CommandLine cmd;
  cmd.AddValue ("simTime", "Total duration of the simulation (in seconds)", simTime);
  cmd.AddValue ("speed", "Speed of the UE (default = 20 m/s)", speed);
  cmd.AddValue ("enbTxPowerDbm", "TX power [dBm] used by HeNBs (default = 46.0)", enbTxPowerDbm);

  cmd.Parse (argc, argv);

  simTime = (double)(numberOfEnbs + 1) * distance / speed;    // recompute the simTime since speed is updated

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");

  lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold",
                                            UintegerValue (30));
  lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset",
                                            UintegerValue (1));

  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create the Internet
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("10Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);


  // Routing of the Internet Host (towards the LTE network)
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  // interface 0 is localhost, 1 is the p2p device
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);


  /*
   * Network topology:
   *
   *      |     + --------------------------------------------------------->
   *      |     UE
   *      |
   *      |               d                   d                   d
   *    y |     |-------------------x-------------------x-------------------
   *      |     |                 eNodeB              eNodeB
   *      |   d |
   *      |     |
   *      |     |                                             d = distance
   *            o (0, 0, 0)                                   y = yForUe
   */

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  enbNodes.Create (numberOfEnbs);
  ueNodes.Create (numberOfUes);

  // Install Mobility Model in eNB
  Ptr<ListPositionAllocator> enbPositionAlloc = CreateObject<ListPositionAllocator> ();
  for (uint16_t i = 0; i < numberOfEnbs; i++)
    {
      Vector enbPosition (distance * (i + 1), distance, 0);
      enbPositionAlloc->Add (enbPosition);
    }
  MobilityHelper enbMobility;
  enbMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");    // constant position == not moving
  enbMobility.SetPositionAllocator (enbPositionAlloc);
  enbMobility.Install (enbNodes);

  // Install Mobility Model in UE
  MobilityHelper ueMobility;
  ueMobility.SetMobilityModel ("ns3::ConstantVelocityMobilityModel");     // moving in constant speed
  ueMobility.Install (ueNodes);
  ueNodes.Get (0)->GetObject<MobilityModel> ()->SetPosition (Vector (0, yForUe, 0));

  std::cout << "------------ speed now has value " << speed << " -------------" << std::endl;

  ueNodes.Get (0)->GetObject<ConstantVelocityMobilityModel> ()->SetVelocity (Vector (speed, 0, 0));

  // Install LTE Devices in eNB and UEs
  Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (enbTxPowerDbm));
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes); // this will install the HO algorithm configured above
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install the IP stack on the UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIfaces;
  ueIpIfaces = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Attach all UEs to the first eNodeB
  for (uint16_t i = 0; i < numberOfUes; i++)
    {
      lteHelper->Attach (ueLteDevs.Get (i), enbLteDevs.Get (0));
    }


  NS_LOG_LOGIC ("setting up applications");

  // Install and start applications on UEs and remote host
  uint16_t dlPort = 10000;
  uint16_t ulPort = 20000;

  // randomize a bit start times to avoid simulation artifacts
  // (e.g., buffer overflows due to packet transmissions happening
  // exactly at the same time)
  Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
  startTimeSeconds->SetAttribute ("Min", DoubleValue (0));
  startTimeSeconds->SetAttribute ("Max", DoubleValue (0.010));

  for (uint32_t u = 0; u < numberOfUes; ++u)
    {
      Ptr<Node> ue = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

      for (uint32_t b = 0; b < numBearersPerUe; ++b)
        {
          ++dlPort;
          ++ulPort;

          ApplicationContainer clientApps;
          ApplicationContainer serverApps;

          NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
          NS_LOG_LOGIC ("UE has address " << ueIpIfaces.GetAddress (u));
          UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
          clientApps.Add (dlClientHelper.Install (remoteHost));
          PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                               InetSocketAddress (Ipv4Address::GetAny (), dlPort));
          serverApps.Add (dlPacketSinkHelper.Install (ue));

          NS_LOG_LOGIC ("installing UDP UL app for UE " << u);
          NS_LOG_LOGIC ("remoteHost has address " << remoteHostAddr);
          UdpClientHelper ulClientHelper (remoteHostAddr, ulPort);
          clientApps.Add (ulClientHelper.Install (ue));
          
          PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory",
                                               InetSocketAddress (Ipv4Address::GetAny (), ulPort));
          
          serverApps.Add (ulPacketSinkHelper.Install (remoteHost));

          Ptr<EpcTft> tft = Create<EpcTft> ();
          EpcTft::PacketFilter dlpf;
          dlpf.localPortStart = dlPort;
          dlpf.localPortEnd = dlPort;
          tft->Add (dlpf);
          EpcTft::PacketFilter ulpf;
          ulpf.remotePortStart = ulPort;
          ulpf.remotePortEnd = ulPort;
          tft->Add (ulpf);
          EpsBearer bearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
          lteHelper->ActivateDedicatedEpsBearer (ueLteDevs.Get (u), bearer, tft);

          Time startTime = Seconds (startTimeSeconds->GetValue ());

          NS_LOG_LOGIC ("start time is set to be " << startTime);

          serverApps.Start (startTime);
          clientApps.Start (startTime);

        } // end for b
    }


  // Add X2 interface
  lteHelper->AddX2Interface (enbNodes);

  // X2-based Handover
  //lteHelper->HandoverRequest (Seconds (0.100), ueLteDevs.Get (0), enbLteDevs.Get (0), enbLteDevs.Get (1));

  // Uncomment to enable PCAP tracing
  // p2ph.EnablePcapAll("lena-x2-handover-measures");

  lteHelper->EnablePhyTraces ();
  lteHelper->EnableMacTraces ();
  lteHelper->EnableRlcTraces ();
  lteHelper->EnablePdcpTraces ();

  Ptr<RadioBearerStatsCalculator> rlcStats = lteHelper->GetRlcStats ();
  rlcStats->SetAttribute ("EpochDuration", TimeValue (Seconds (1.0)));
  Ptr<RadioBearerStatsCalculator> pdcpStats = lteHelper->GetPdcpStats ();
  pdcpStats->SetAttribute ("EpochDuration", TimeValue (Seconds (1.0)));

  FlowMonitorHelper flowHelper;
  flowHelper.InstallAll();


  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/ConnectionEstablished",
                   MakeCallback (&NotifyConnectionEstablishedEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionEstablished",
                   MakeCallback (&NotifyConnectionEstablishedUe));

  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverStart",
                   MakeCallback (&NotifyHandoverStartEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart",
                   MakeCallback (&NotifyHandoverStartUe));
  
  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
                   MakeCallback (&NotifyHandoverEndOkEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverEndOk",
                   MakeCallback (&NotifyHandoverEndOkUe));

  /////////////////////////////// my additional trace "hook" //////////////////////////////
  
  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/ConnectionReconfiguration",
  //                 MakeCallback (&NotifyConnectionReconfigurationEnb));

  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/RecvMeasurementReport",
  //                 MakeCallback (&NotifyRecvMeasurementReport));

  //Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionReconfiguration",
  //                 MakeCallback (&NotifyConnectionReconfigurationUe));

  //Config::Connect ("/NodeList/*/NodeList/*/ApplicationList/*/$ns3::UdpServer",
  //                 MakeCallback (&NotifyUdpServerRx));

  Config::Connect ("/NodeList/*/DeviceList/*/$ns3::LteNetDevice/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportUeMeasurements",
                   MakeCallback (&NotifyUeMeasurements));

  Config::Connect ("/NodeList/*/DeviceList/*/$ns3::LteNetDevice/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
                   MakeCallback (&NotifySinrReport));

  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/RadioLinkFailure",
                   MakeCallback (&RadioLinkFailure));
  
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/PhySyncDetection", 
                   MakeCallback (&NotifyPhySync));


  /////////////////////////////////////////////////////////////////////////////////////////

  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

  // GtkConfigStore config;
  // config.ConfigureAttributes ();

  flowHelper.SerializeToXmlFile("scratch/flowmonitor.txt", true, true);

  Simulator::Destroy ();

  return 0;
}
