#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

from random import sample

if __name__ == '__main__':

	f = open("rsrp.txt", 'r')
	contents = f.readlines()
	f.close()

	time_list = []
	rsrp_1_list = []
	rsrp_2_list = []

	HO_time = 0.0

	for line in contents:
			
		strings = line.split(' ')
			
		if len(strings) == 5:
			if float(strings[1]) == 0.0:
				HO_time = float(strings[0])
				continue
			else:
				time_list.append(float(strings[0]))
				rsrp_1_list.append(float(strings[1]))
				rsrp_2_list.append(float(strings[3]))


	print ("------------ ho time ------------")
	print (HO_time)

	# actual drawing
	fig, ax = plt.subplots()

	ax.plot(time_list, rsrp_1_list)
	ax.plot(time_list, rsrp_2_list)
	ax.axvline(x = HO_time, color='red')


	ax.set(xlabel='time (s)', ylabel='RSRP (dBm)', title='RSRP')
	ax.grid()

	fig.savefig("rsrp.png")

	