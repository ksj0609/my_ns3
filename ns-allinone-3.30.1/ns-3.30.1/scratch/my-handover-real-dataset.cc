/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2013 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Manuel Requena <manuel.requena@cttc.es>
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store-module.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/geographic-positions.h"
#include "ns3/netanim-module.h"
#include <math.h>
#include <fstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("HandoverRealData");

/*
 * Function to convert "range" to transmit power of eNB
 * Reversing the FriisPropagationLossModel::DoCalcRxPower() function
 *
 *                           lambda^2
 * tx = rx -  10 log10 (-------------------)
 *                       (4 * pi * d)^2 * L
 *
 * tx, rx = power in dBm
 * lambda = wavelength in meters, calculated from dlfrequency
 * d = distance in meters, calculated from range
 * L = system loss, default value = 1.0
 *
 *
 *
*/ 
double convertTxPower(double range, double dlfreq){

  double lower_bound_dbm = -15.0;     // this is the "bound" of cell range
  
  double lambda = (3 * pow(10, 8)) / (dlfreq * pow(10, 6));
  double numerator = lambda * lambda;

  double denominator = 16 * M_PI * M_PI * range * range;

  double lossDb = -10 * log10 (numerator / denominator);

  NS_LOG_LOGIC ("lambda = " << lambda << ", range = " << range << ", loss db = " << lossDb);

  return lower_bound_dbm+lossDb;
}

struct Measurement{
  double timestamp;
  double rsrp;
  double rsrq;
  double sinr;
};

void
NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti)
{
  std::cout << "============================================" << std::endl
            << context << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << " UE IMSI " << imsi << std::endl
            << ": connected to CellId " << cellid << std::endl
            << " with RNTI " << rnti << std::endl
            << "============================================"<< std::endl;
}

void
NotifyHandoverStartUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellid,
                       uint16_t rnti,
                       uint16_t targetCellId)
{
  std::cout << "==================================================" << std::endl
            << context << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << " UE IMSI " << imsi << std::endl
            << ": previously connected to CellId " << cellid << std::endl
            << " with RNTI " << rnti << std::endl
            << ", doing handover to CellId " << targetCellId <<std::endl
            << "==================================================" << std::endl;
}

void
NotifyHandoverEndOkUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellid,
                       uint16_t rnti)
{
  std::cout << "==================================================" << std::endl
            << context << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << " UE IMSI " << imsi << std::endl
            << ": successful handover to CellId " << cellid << std::endl
            << " with RNTI " << rnti << std::endl
            << "==================================================" << std::endl;
}

void
NotifyConnectionEstablishedEnb (std::string context,
                                uint64_t imsi,
                                uint16_t cellid,
                                uint16_t rnti)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": successful connection of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;


}

void
NotifyHandoverStartEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti,
                        uint16_t targetCellId)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": start handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << " to CellId " << targetCellId
            << std::endl;

  std::ofstream outfile;
  outfile.open("scratch/rsrp.txt", std::ofstream::out | std::ofstream::app);

  outfile << Simulator::Now ().GetSeconds () << " " << 0 << " " << 0 << " " << 0 << " " << 0 << std::endl;

  outfile.flush();
  outfile.close();
}

void
NotifyHandoverEndOkEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti)
{
  std::cout << context
            << " eNB CellId " << cellid
            << ": completed handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;
}

/**
   * The `ConnectionReconfiguration` trace source. Fired upon RRC connection
   * reconfiguration. Exporting IMSI, cell ID, and RNTI.
   
  TracedCallback<uint64_t, uint16_t, uint16_t> m_connectionReconfigurationTrace;
*/
void
NotifyConnectionReconfigurationUe (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti)
{
  std::cout << "============================================================" << std::endl
            << '\t' << context << std::endl
            << '\t' << "eNB CellId = " << cellid << std::endl
            << '\t' << "UE IMSI = " << imsi << " received reconfiguration" << std::endl
            << '\t' << "RNTI = " << rnti << std::endl
            << "============================================================" << std::endl;
}

/**
   * The `RecvMeasurementReport` trace source. Fired when measurement report is
   * received. Exporting IMSI, cell ID, and RNTI.
   
  TracedCallback<uint64_t, uint16_t, uint16_t, LteRrcSap::MeasurementReport> m_recvMeasurementReportTrace;
*/
void
NotifyRecvMeasurementReport (std::string context,
                        uint64_t imsi,
                        uint16_t cellid,
                        uint16_t rnti, 
                        LteRrcSap::MeasurementReport)
{
  std::cout << "============================================================" << std::endl
            << '\t' << context << std::endl
            << '\t' << "eNB CellId = " << cellid << std::endl
            << '\t' << "Received measurement report from UE IMSI = " << imsi << std::endl
            << '\t' << "RNTI = " << rnti << std::endl
            << "============================================================" << std::endl;
}

void
NotifyUeMeasurements (std::ostream *os,
                      //std::ostream *os2,
                      std::string context, 
                      uint16_t rnti,
                      uint16_t cell_id, 
                      double rsrp,
                      double rsrq, 
                      bool is_serving, 
                      uint8_t m_component_carrier_id)
{
  std::cout << "------------------------------------" << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << "reporting cell signal strength of rnti " << (uint16_t)rnti << ", cell id " << (uint16_t) cell_id << std::endl
            << "rsrp = " << rsrp << ", rsrq = " << rsrq << std::endl
            << "is_serving_cell = " << is_serving << std::endl
            << "------------------------------------" << std::endl;
  
  if (is_serving){
    *os << Simulator::Now().GetSeconds() << ","
        << rsrp << ","
        << rsrq << std::endl;
  }

  //std::ofstream outfile;
  //outfile.open("scratch/rsrp.txt", std::ofstream::out | std::ofstream::app);

  /*
  if (cell_id == 1){
    *os2 << Simulator::Now ().GetSeconds () << "," << rsrp << "," << rsrq << ",";
  } 
  else if (cell_id == 2){
    *os2 << rsrp << "," << rsrq << std::endl;
  }
  */

  //outfile.flush();
  //outfile.close();
}

/*
static void
CourseChange (std::ostream *os, std::string context, Ptr<const MobilityModel> mobility)
{
  Vector pos = mobility->GetPosition (); // Get position
  Vector vel = mobility->GetVelocity (); // Get velocity

  // Prints position and velocities
  *os << Simulator::Now () << " POSITION: pos.x=" << pos.x << ", pos.y=" << pos.y
      << ", pos.z=" << pos.z << "; VELOCITY: vel.x:" << vel.x << ", vel.y=" << vel.y
      << ", vel.z=" << vel.z << std::endl;
}
*/

 /**
   * The `ReportCurrentCellRsrpSinr` trace source. Trace information regarding
   * RSRP and average SINR (see TS 36.214). Exporting cell ID, RNTI, RSRP, and
   * SINR. Moreover it reports the m_componentCarrierId.
   * 
   * TracedCallback<uint16_t, uint16_t, double, double, uint8_t> m_reportCurrentCellRsrpSinrTrace;
   */
void 
NotifyRsrpSinr (std::string context, 
                uint16_t cell_id,
                uint16_t rnti,
                double rsrp,
                double sinr,
                uint8_t m_component_carrier_id)
{
  std::cout << "--------------" << std::endl
            << "at time " << Simulator::Now ().GetSeconds () << std::endl
            << "reporting for cell " << cell_id << std::endl
            << "rsrp = " << 10*log10(rsrp) << std::endl
            << "sinr = " << sinr << std::endl
            << "--------------" << std::endl;

}

int
main (int argc, char *argv[])
{
  LogLevel logLevel = (LogLevel)(LOG_PREFIX_ALL | LOG_LOGIC);
  LogComponentEnable ("HandoverRealData", logLevel);
  //LogComponentEnable ("Ns2Mobility3DHelper", logLevel);
  //LogComponentEnable ("A2A4RsrqHandoverAlgorithm", logLevel);
  //LogComponentEnable ("A3RsrpHandoverAlgorithm", logLevel);
  //LogComponentEnable ("LteEnbRrc", logLevel);
  //LogComponentEnable ("LteUeRrc", logLevel);
  //LogComponentEnable ("LteEnbPhy", logLevel);
  //LogComponentEnable ("LteUePhy", logLevel);
  //LogComponentEnable ("LteSpectrumPhy", logLevel);
  LogComponentEnable ("PacketSink", logLevel);
  LogComponentEnable ("UdpClient", logLevel);

  //std::cout << "start" << std::endl;
  std::string file_dir;
  std::string exp_number;
  std::string speed;
  double tx_power_add;
  uint64_t offset;
  uint16_t WindowNumber;
  uint16_t Rewards;

  CommandLine cmd;
  cmd.AddValue("file_dir", "data file directory", file_dir);
  cmd.AddValue("exp_number", "No of experiment", exp_number);
  cmd.AddValue("speed", "speed of the train", speed);
  cmd.AddValue("tx_power_add", "Additional power", tx_power_add);
  cmd.AddValue("offset", "A4 offset value", offset);
  cmd.AddValue("WindowNumber", "running window size", WindowNumber);
  cmd.AddValue("Rewards", "neighbor cell rewards", Rewards);

  cmd.Parse(argc, argv);

  std::cout << "file dir = " << file_dir << std::endl;
  std::cout << "exp number = " << exp_number << std::endl;
  std::cout << "speed = " << speed << std::endl;
  std::cout << "power add = " << tx_power_add << std::endl;
  std::cout << "offset = " << (uint64_t)offset << std::endl;
  std::cout << "window number = " << (uint16_t)WindowNumber << std::endl;
  std::cout << "rewards = " << (uint16_t)Rewards << std::endl;

  //////////////// First read the csv file to collect earfcn and dlfreq info /////////////
  NS_LOG_LOGIC ("---------- Reading eNB infomation -------------");

  std::string route_file;
  route_file = "scratch/"+file_dir+"route_"+exp_number+".csv";

  double eNBDlfreq;       // downlink frequency of eNBs
  uint32_t earfcn;        // downlink earfcn of eNBs

  // first read earfcn values
  std::ifstream fin;
  std::string line, word;           // read file into lines, then into words
  std::vector<std::string> row;     // one row is one line, with X words

  fin.open(route_file, std::ios::in);

  if(fin){
    std::cout << "read UE file success" << std::endl;
  }
  else{
    std::cout << "read UE file fail" << std::endl;
  }

  std::getline(fin, line);  // skip the first line

  // since there may be some missing data, we need to keep reading
  // until it's NOT empty
  bool earfcn_exists = false;
  while (!earfcn_exists){
    std::getline(fin, line);
    std::stringstream ss(line);

    row.clear();
    while(getline(ss, word, ',')){
      row.push_back(word);
    }
    if (row.size() == 11){
      if (!row[5].empty() && !row[6].empty()){
        NS_LOG_LOGIC ("this both entries are not empty");
        earfcn = stoi(row[5]);
        eNBDlfreq = stod(row[6]);
        NS_LOG_LOGIC ("earfcn = " << earfcn << ", freq = " << eNBDlfreq);
        earfcn_exists = true;
      }
      else
        NS_LOG_LOGIC ("exist empty entries, skip this line");
    }
  }
  row.clear();
  fin.close();

  //////////////// First read the csv file to collect eNodeB info /////////////
  std::string enb_file;
  enb_file = "scratch/"+file_dir+"enb_"+exp_number+".csv";

  size_t numberOfEnbs;
  std::vector<Vector> eNBPositions;
  std::vector<double> eNBRanges;
  std::vector<double> eNBTxPwerDbm;     // this needs to be calculated based on ranges and dlfreq

  fin.open(enb_file, std::ios::in);

  if(fin){
    std::cout << "read eNB success" << std::endl;
  }
  else{
    std::cout << "read eNB fail" << std::endl;
  }

  // CSV format: index, latitude, longitude, range
  // read through the csv file and store useful information
  std::vector<double> lat_list;
  std::vector<double> lon_list;

  std::getline(fin, line); // skip the first line

  // read the rest file line by line
  while (fin >> line) { 

    row.clear();
    std::stringstream ss(line);

    while (getline(ss, word, ',')) {
      //std::cout << word << std::endl;
      row.push_back(word);
    }
    if (row.size() == 6){
      std::cout << "this row has lat = " << stod(row[2]) << ", lon = " << stod(row[3]) << std::endl;
      lat_list.push_back(stod(row[2]));
      lon_list.push_back(stod(row[3]));
      eNBRanges.push_back(stod(row[4]));
    }  
  }
  row.clear();
  fin.close();

  // then convert to Vector coordinates
  for (size_t i = 0; i < lat_list.size(); i++){
    eNBPositions.push_back(GeographicPositions::GeographicToCartesianCoordinates(lat_list[i], 
                                                                                 lon_list[i], 
                                                                                 0.0,  // distance above the ground
                                                                                 GeographicPositions::WGS84));
  }

  /*
  for (size_t i = 0; i < lat_list.size(); i++){
    eNBTxPwerDbm.push_back(convertTxPower(eNBRanges[i], eNBDlfreq));
  }
  */
  for (size_t i = 0; i < lat_list.size(); i++){
    eNBTxPwerDbm.push_back(eNBRanges[i]);
  }

  for (size_t i = 0; i < lat_list.size(); i++){
    NS_LOG_LOGIC ("Tx power for eNB " << i << " = " << eNBTxPwerDbm[i]);
  }

  numberOfEnbs = eNBPositions.size();

  std::cout << "number of eNBs = " << numberOfEnbs << std::endl;

  for (size_t i = 0; i <= eNBPositions.size(); i++){
    std::cout << "position = <" << eNBPositions[i].x << ", " 
              << eNBPositions[i].y << ", " << eNBPositions[i].z << ">" << std::endl;
  }

  ////////////////////////// then read the trace file to identify simulation time //////////////////
  // first read the "train_movement_3d.txt" file to generate simulation time
  // it is the last line's "at" value
  std::string real_meas_file;
  real_meas_file = "scratch/"+file_dir+"real_meas_exp_"+exp_number
                           +"_speed_"+speed+"_power_"+std::to_string(int(tx_power_add))+"_offset_"+std::to_string(offset)
                           +"_window_"+std::to_string(WindowNumber)+"_rewards_"+std::to_string(Rewards)+".csv";

  double simTime;

  fin.open(real_meas_file, std::ios::in);
  if(fin){
    std::cout << "read real meas success" << std::endl;
  }
  else{
    std::cout << "read real meas fail" << std::endl;
  }

  fin.seekg(-2, std::ios_base::end);                // go to one spot before the EOF

  bool keepLooping = true;
  
  while(keepLooping) {
    
    char ch;
    fin.get(ch);                            // Get current byte's data

    if((int)fin.tellg() <= 1) {             // If the data was at or before the 0th byte
      fin.seekg(0);                         // The first line is the last line
      keepLooping = false;                  // So stop there
    }
    else if(ch == '\n') {                     // If the data was a newline
      keepLooping = false;                    // Stop at the current position.
    }
    else {                                      // If the data was neither a newline nor at the 0 byte
      fin.seekg(-2, std::ios_base::cur);        // Move to the front of that data, then to the front of the data before it
    }
  }

  std::string lastLine;            
  getline(fin, lastLine);                                // Read the current line
  
  std::stringstream ss(lastLine);
  std::string lastWord;

  //std::cout << "---------- last line = " << lastLine << "-------" << std::endl;

  // it's the first word, thus call getline() one time
  getline(ss, lastWord, ',');
  //getline(ss, lastWord, ',');
  //getline(ss, lastWord, ',');
  
  //std::cout << "-----------------lastWord = " << "-------" << lastWord << std::endl;

  fin.close();

  simTime = stod(lastWord);

  std::cout << "sim time = " << simTime << std::endl;

  uint16_t numberOfUes = 1;       // only one train
  uint16_t numBearersPerUe = 1;   // set 0 if we don't care about the app layer transmission

  //double enbTxPowerDbm = 62.0; // this should be assigned for each eNB, inferred from "range" value

  ///////////////////////// setup LTE protocol parameters //////////////////////////////
  // change some default attributes so that they are reasonable for
  // this scenario, but do this before processing command line
  // arguments, so that the user is allowed to override these settings
  Config::SetDefault ("ns3::UdpClient::Interval", TimeValue (MilliSeconds (2)));
  Config::SetDefault ("ns3::UdpClient::PacketSize", UintegerValue(500));
  Config::SetDefault ("ns3::UdpClient::MaxPackets", UintegerValue (1000000));
  Config::SetDefault ("ns3::LteHelper::UseIdealRrc", BooleanValue (false));

  Config::SetDefault ("ns3::LteUePhy::Qout", DoubleValue (-1));
  Config::SetDefault ("ns3::LteUeRrc::T310", TimeValue (MilliSeconds (50)));
  Config::SetDefault ("ns3::LteUeRrc::N310", UintegerValue(1));

  Config::SetDefault ("ns3::RadioEnvironmentMapHelper::Earfcn", UintegerValue(earfcn));

  //////////////////////// create EPC and remote host ////////////////////////////////
  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);
  lteHelper->SetSchedulerType ("ns3::RrFfMacScheduler");

  // handover algorithms, this is the "research" part
  //lteHelper->SetHandoverAlgorithmType ("ns3::A3RsrpHandoverAlgorithm");


  /*
    A2A4 algorithm contains (1) A2: when cerving cell drops below a threshold Ms+Hys < Thresh
                            (2) A4: when a neighbor cell strongers than a thershold Mn+Ofn+Ocn-Hys > Thresh
  */
  lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold", UintegerValue (30));    // 30 dB?
  lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset", UintegerValue (offset));
  lteHelper->SetHandoverAlgorithmAttribute ("WindowNumber", UintegerValue(WindowNumber));
  lteHelper->SetHandoverAlgorithmAttribute ("Rewards", UintegerValue(Rewards));

  // downlink frequency
  lteHelper->SetEnbDeviceAttribute("DlEarfcn", UintegerValue(earfcn));
  lteHelper->SetUeDeviceAttribute("DlEarfcn", UintegerValue(earfcn));

  // path-loss model, here shall be inferred by "RSRP, RSRQ, SINR" values
  // does the path loss model need to be configured with frequency? or it can figure out by itself?
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  // P-GW is the EPC gateway to the Internet
  Ptr<Node> pgw = epcHelper->GetPgwNode ();
  NS_LOG_LOGIC ("id of the P-GW node = " << pgw->GetId());    // the pgw node has id == 0

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);
  NS_LOG_LOGIC ("id of the remote host = " << remoteHost->GetId());   // the remote host has id == 3

  // Create the Internet
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Mb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  // Routing of the Internet Host (towards the LTE network)
  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  // interface 0 is localhost, 1 is the p2p device
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  /////////////////////// now configure the UE and eNB nodes, their loacations and mobility ////////////////
  NodeContainer ueNodes;
  NodeContainer enbNodes;
  ueNodes.Create(numberOfUes);
  enbNodes.Create(numberOfEnbs);

  // Install Mobility Model in eNBs
  Ptr<ListPositionAllocator> enbPositionAlloc = CreateObject<ListPositionAllocator> ();
  for (uint16_t i = 0; i < numberOfEnbs; i++)
  {
    enbPositionAlloc->Add(eNBPositions[i]);
  }

  MobilityHelper enbMobility;
  enbMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");    // constant position == not moving
  enbMobility.SetPositionAllocator(enbPositionAlloc);
  enbMobility.Install(enbNodes);

  // Install Mobility Model in UE
  std::string movement_file;
  movement_file = "scratch/"+file_dir+"movement_"+exp_number+".txt";
  Ns2Mobility3DHelper ueMobility = Ns2Mobility3DHelper(movement_file);
  ueMobility.Install(ueNodes);    // this installs all the UE nodes?

  // Install LTE Devices in eNB and UEs
  // Config::SetDefault ("ns3::LteEnbPhy::TxPower", DoubleValue (enbTxPowerDbm));
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes); // this will install the HO algorithm configured above
  
  unsigned int idx = 0;
  // configure eNB device txPower attributes
  for (NetDeviceContainer::Iterator i = enbLteDevs.Begin(); i != enbLteDevs.End(); i++){

    //double tx_power_add = 65;
    double assign_value = eNBTxPwerDbm[idx]+tx_power_add;

    Ptr<LteEnbPhy> enbPhy = DynamicCast<LteEnbNetDevice>(*i)->GetPhy();
    NS_LOG_LOGIC ("assign " << assign_value << " to this eNB");
    enbPhy->SetAttribute("TxPower", DoubleValue(assign_value));
    idx++;
  }

  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install the IP stack on the UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIfaces;
  ueIpIfaces = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Attach all UEs to the first eNodeB
  // Here the "first" may NOT be correct! because PCI is sorted randomly
  for (uint16_t i = 0; i < numberOfUes; i++)
  {
    lteHelper->Attach(ueLteDevs.Get (i), enbLteDevs.Get (0));
  }

  ////////////////////// here configure Application layer traffic transmission ///////////////////
  // Install and start applications on UEs and remote host
  uint16_t dlPort = 10000;
  uint16_t ulPort = 20000;

  // randomize a bit start times to avoid simulation artifacts
  // (e.g., buffer overflows due to packet transmissions happening
  // exactly at the same time)
  Ptr<UniformRandomVariable> startTimeSeconds = CreateObject<UniformRandomVariable> ();
  startTimeSeconds->SetAttribute ("Min", DoubleValue (0));
  startTimeSeconds->SetAttribute ("Max", DoubleValue (0.1));

  for (uint32_t u = 0; u < numberOfUes; ++u)
  {
      Ptr<Node> ue = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);

      for (uint32_t b = 0; b < numBearersPerUe; ++b)
      {
        ++dlPort;
        ++ulPort;

        ApplicationContainer clientApps;
        ApplicationContainer serverApps;

        NS_LOG_LOGIC ("installing UDP DL app for UE " << u);
        NS_LOG_LOGIC ("UE has address " << ueIpIfaces.GetAddress (u));
        UdpClientHelper dlClientHelper (ueIpIfaces.GetAddress (u), dlPort);
        clientApps.Add (dlClientHelper.Install (remoteHost));
        PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                               InetSocketAddress (Ipv4Address::GetAny (), dlPort));
        serverApps.Add (dlPacketSinkHelper.Install (ue));

        NS_LOG_LOGIC ("installing UDP UL app for UE " << u);
        NS_LOG_LOGIC ("remoteHost has address " << remoteHostAddr);
        UdpClientHelper ulClientHelper (remoteHostAddr, ulPort);
        clientApps.Add (ulClientHelper.Install (ue));
          
        PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory",
                                               InetSocketAddress (Ipv4Address::GetAny (), ulPort));
          
        serverApps.Add (ulPacketSinkHelper.Install (remoteHost));

        Ptr<EpcTft> tft = Create<EpcTft> ();
        EpcTft::PacketFilter dlpf;
        dlpf.localPortStart = dlPort;
        dlpf.localPortEnd = dlPort;
        tft->Add (dlpf);
        EpcTft::PacketFilter ulpf;
        ulpf.remotePortStart = ulPort;
        ulpf.remotePortEnd = ulPort;
        tft->Add (ulpf);
        EpsBearer bearer (EpsBearer::NGBR_VIDEO_TCP_DEFAULT);
        lteHelper->ActivateDedicatedEpsBearer (ueLteDevs.Get (u), bearer, tft);

        Time startTime = Seconds (startTimeSeconds->GetValue ());
        Time endTime = Seconds(simTime)-Seconds(1)-Seconds (startTimeSeconds->GetValue());

        NS_LOG_LOGIC ("start time is set to be " << startTime);
        NS_LOG_LOGIC ("end time is set to be " << endTime);

        serverApps.Start (startTime);
        clientApps.Start (startTime);

        serverApps.Stop(endTime);
        clientApps.Stop(endTime);

      } // end for b
  }

  FlowMonitorHelper flowHelper;
  flowHelper.Install(ueNodes);
  flowHelper.Install(remoteHostContainer);


  // Add X2 interface
  lteHelper->AddX2Interface (enbNodes);

  lteHelper->EnablePhyTraces ();
  lteHelper->EnableMacTraces ();
  lteHelper->EnableRlcTraces ();
  lteHelper->EnablePdcpTraces ();

  // this OS is used to output received power
  std::string logFile;
  logFile = "scratch/"+file_dir+"sim_meas_exp_"+exp_number
                           +"_speed_"+speed+"_power_"+std::to_string(int(tx_power_add))+"_offset_"+std::to_string(offset)
                           +"_window_"+std::to_string(WindowNumber)+"_rewards_"+std::to_string(Rewards)+".csv";

  std::cout << "writing simulated rsrp to file " << logFile << std::endl;

  std::ofstream os;
  os.open(logFile.c_str());
  os << "sim_time,rsrp,rsrq" << std::endl;   // header

  /*
  // this OS is used to output position
  std::string logFile2 = "scratch/long_dataset/course_change_"+exp_number+".txt";
  std::ofstream os2;
  os2.open(logFile2.c_str());
  */

  /*
  // this OS is used to output rsrp/rsrq of both cells
  std::string logFile3 = "scratch/long_dataset/rsrp_two_cells_"+exp_number+".csv";
  std::ofstream os3;
  os3.open(logFile3.c_str());
  os3 << "sim_time,rsrp_1,rsrq_1,rsrp_2,rsrq_2" << std::endl;   // header
  */

  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/ConnectionEstablished",
  //                 MakeCallback (&NotifyConnectionEstablishedEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionEstablished",
                   MakeCallback (&NotifyConnectionEstablishedUe));

  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverStart",
  //                 MakeCallback (&NotifyHandoverStartEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart",
                   MakeCallback (&NotifyHandoverStartUe));
  
  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
  //                 MakeCallback (&NotifyHandoverEndOkEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverEndOk",
                   MakeCallback (&NotifyHandoverEndOkUe));

  // UE send measurement report to eNB
  Config::Connect ("/NodeList/*/DeviceList/*/$ns3::LteNetDevice/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportUeMeasurements",
                   MakeBoundCallback (&NotifyUeMeasurements, &os));

  //Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionReconfiguration",
  //                 MakeCallback (&NotifyConnectionReconfigurationUe));

  // eNB received measurement from UE, decide whether to start Handover
  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/RecvMeasurementReport",
  //                 MakeCallback (&NotifyRecvMeasurementReport));

  // Mobility trace source of the UE 
  
  //Config::Connect ("/NodeList/*/$ns3::MobilityModel/CourseChange",
  //                MakeBoundCallback (&CourseChange, &os2));
  
  //Config::Connect ("/NodeList/*/DeviceList/*/$ns3::LteNetDevice/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
  //                MakeCallback(&NotifyRsrpSinr));

  Simulator::Stop (Seconds (simTime));

  //std::string animFile = "scratch/test.xml";
  //AnimationInterface anim(animFile);
  
  NS_LOG_LOGIC ("================ start simulation ===============");

  Simulator::Run ();

  // GtkConfigStore config;
  // config.ConfigureAttributes ();

  std::string logFile4;
  logFile4 = "scratch/"+file_dir+"flow_exp_"+exp_number
                           +"_speed_"+speed+"_power_"+std::to_string(int(tx_power_add))+"_offset_"+std::to_string(offset)
                           +"_window_"+std::to_string(WindowNumber)+"_rewards_"+std::to_string(Rewards)+".xml";

  flowHelper.SerializeToXmlFile(logFile4, true, true);

  Simulator::Destroy ();
  os.close();
  //os2.close();

  return 0;
}
