#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np

if __name__ == '__main__':

	"""
	for s in [20, 30, 40]:
		for i in range(0, 555):
			subprocess.run('python3 real_experiment.py %d %d' %(i, s), shell=True)
	"""

	#offsets = [1]

	speed = ['20']
	offsets = [1, 2]
	windows = [1]
	rewards = [0]

	for s in speed:
		for o in offsets:
			for w in windows:
				for r in rewards:
					for i in range(0, 2):
						subprocess.run('python3 real_experiment.py new_long_dataset/ %d %s 65 %d %d %d' %(i, s, o, w, r), shell=True)
	
