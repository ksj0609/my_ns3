#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from random import sample


if __name__ == '__main__':

	# first read all the CLI arguments and save them
	par_name = ""
	par_vals = []

	par_name = sys.argv[1]

	for i in range(2, len(sys.argv)):
		par_vals.append(sys.argv[i])

	#print(par_name)
	#print(par_vals)

	# assume format of rsrp filenames are in the format: rsrp_name_value.txt
	# and each .txt file has the format [time rsrp1 rsrq1 rsrp2 rsrq2]

	rsrp_data_lists = [] 	# a list of 2D arrays, each one corresponds to one parameter value
	filename = ""
	
	for val in par_vals:
		
		filename = "rsrp_"+par_name+"_"+val+".txt"
		f = open(filename, 'r')
		contents = f.readlines()
		f.close()

		rsrp_data = [[] for i in range(6)]	# the last one has only one element, to be HO time

		#print(contents)
		for line in contents:
			
			strings = line.split(' ')
			
			if len(strings) == 5:
				if float(strings[1]) == 0.0:
					rsrp_data[5].append(float(strings[0]))
					continue
				else:
					rsrp_data[0].append(float(strings[0]))
					rsrp_data[1].append(float(strings[1]))
					rsrp_data[2].append(float(strings[2]))
					rsrp_data[3].append(float(strings[3]))
					rsrp_data[4].append(float(strings[4]))

		rsrp_data_lists.append(rsrp_data)

	#print(rsrp_data_lists)

	# now start drawing two figures: rsrp and rsrq

	# first, among all the values, decide which one has the most values
	x_axis_array = []
	for i in range(0, len(par_vals)):
		if len(x_axis_array) < len(rsrp_data_lists[i][0]):
			x_axis_array = rsrp_data_lists[i][0]

	#print(x_axis_array)

	# then, construct line style attribute and randomly assigned to each value
	line_styles = [':', '-.', '--', '-']
	line_styles = sample(line_styles, len(par_vals))

	# then, append empty values to the ones with less length
	for i in range(0, len(par_vals)):
		for _ in range(len(x_axis_array)-len(rsrp_data_lists[i][0])):
			rsrp_data_lists[i][1].append(np.nan)
			rsrp_data_lists[i][2].append(np.nan)
			rsrp_data_lists[i][3].append(np.nan)
			rsrp_data_lists[i][4].append(np.nan)

	# actual drawing
	fig, ax = plt.subplots()

	for i in range(0, len(par_vals)):

		ax.plot(x_axis_array, rsrp_data_lists[i][1], label="cell_1,"+par_vals[i], color="blue", linestyle=line_styles[i])
		ax.plot(x_axis_array, rsrp_data_lists[i][3], label="cell_2,"+par_vals[i], color="orange", linestyle=line_styles[i])
		ax.axvline(x = rsrp_data_lists[i][5][0], color='red', linestyle=line_styles[i])

	ax.set(xlabel='time (s)', ylabel='RSRP (dBm)', title='RSRP of different '+par_name)
	ax.legend(loc='center right', ncol=1, fontsize=10)
	ax.grid()

	fig.savefig("rsrp_"+par_name+".png")

	ax.clear()

	# actual drawing
	fig, ax = plt.subplots()

	for i in range(0, len(par_vals)):

		ax.plot(x_axis_array, rsrp_data_lists[i][2], label="cell_1,"+par_vals[i], color="blue", linestyle=line_styles[i])
		ax.plot(x_axis_array, rsrp_data_lists[i][4], label="cell_2,"+par_vals[i], color="orange", linestyle=line_styles[i])
		ax.axvline(x = rsrp_data_lists[i][5][0], color='red', linestyle=line_styles[i])

	ax.set(xlabel='time (s)', ylabel='RSRQ (dB)', title='RSRQ of different '+par_name)
	ax.legend(loc='center right', ncol=1, fontsize=10)
	ax.grid()

	fig.savefig("rsrq_"+par_name+".png")

	ax.clear()
	