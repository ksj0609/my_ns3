#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np

"""
This is a script to automate expriment process. It assumes the following:
	1. Each experiment changes one parameter
	2. The NS3 script my-x2-handover-measurment.cc takes the parameter as input, 
		which is enabled by the "cmd.addValue()" function
	3. The experiment results is drawn in one figure, with x-axis being time
		and y-axis being RSRP/RSRQ
"""

"""
Input argument from CLI:
	1. parameter name (e.g., speed, simTime, etc.)
	2. a list of parameter values, separated by space (e.g, 100, 200, 300, etc.)
"""

"""
Process:
	1. From the input values, [val 1, val 2, ..., val n], conduct n experiments
	2. For each exp:
		(1) Execute my-x2-handover-measure program with a paramter value
		(2) Re-name the rsrp.txt to rsrp_name_value.txt according to the parameter name and value
	3. After all the .txt file generated, execute the drawing function with the file names as arguments
"""

if __name__ == '__main__':

	# first read all the CLI arguments and save them
	par_name = ""
	par_vals = []

	par_name = sys.argv[1]

	for i in range(2, len(sys.argv)):
		par_vals.append(sys.argv[i])

	# second, execute the simulation program and re-name the rsrp file
	f = open("log.txt", "w+")

	par_command = ""
	file_name = ""

	for val in par_vals:
		
		par_command = "my-x2-handover-measures --"+par_name+"="+val
		file_name = "rsrp_"+par_name+"_"+val+".txt"
		
		subprocess.run(["rm", "rsrp.txt"])
		print("executing experiment with "+par_name+" = "+val)
		subprocess.call(["../waf", "--run", par_command], stdout=f)
		print("change rsrp file name")
		subprocess.run(["mv", "rsrp.txt", file_name])
	

	f.close() 	# log contains all the running output, not just the last one

	# at last, pass the drawing script with the parameter name and values
	draw_command = []
	draw_command.append("./draw_rsrp_results.py")
	for i in range(1, len(sys.argv)):
		draw_command.append(sys.argv[i])

	print("Now start draw figures")

	subprocess.run(draw_command)

