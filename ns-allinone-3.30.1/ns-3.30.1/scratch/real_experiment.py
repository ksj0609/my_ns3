#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np

"""
Experiment number past as argument

This is a script to automate expriment process, steps are:
	
	(1) Copy two files to NS3 directory
	(2) Generate mobility trace file and real_measurements and estimate_measurements files
	(4) Execute simulation
	(5) Generate log files and rsrp comparison 
"""

"""
Arguments:
	0. self
	1. exp_number
	2. speed 			// if it's orginal speed, then use 'original'
	3. tx_power_add
	4. offset
"""
if __name__ == '__main__':

	my_parser = argparse.ArgumentParser()

	my_parser.add_argument('file_dir', type=str)
	my_parser.add_argument('exp_number', type=int)
	my_parser.add_argument('speed', type=str)				# speed could be 20, 30, 40, ... or 'original'
	my_parser.add_argument('tx_power_add', type=int)
	my_parser.add_argument('offset', type=int)
	my_parser.add_argument('window_number', type=int)
	my_parser.add_argument('rewards', type=int)

	args = my_parser.parse_args()

	print (args.exp_number)
	print (args.speed)

	""" This code only do once, copy the remote data files to local 

	dataset_dir = "/home/xin/lte_data_scripts/dataset/long_results/"
	local_dataset_dir = "./long_dataset/"
	local_figure_dir = "./long_figures/"

	route_file = dataset_dir+"route_%d.csv" %(args.exp_number)
	enb_file = dataset_dir+"enb_%d.csv" %(args.exp_number)

	# copy the file from the dataset directly to local
	subprocess.run(["cp", route_file, local_dataset_dir])
	subprocess.run(["cp", enb_file, local_dataset_dir])	

	"""

	# generate mobility trace file
	print ("------- generate mobility trace ---------\n")
	subprocess.run(['../waf', '--run', './read_train_route --file_dir=%s --exp_number=%d --speed=%s --tx_power_add=%d --offset=%d --WindowNumber=%d --Rewards=%d' 
						%(args.file_dir, args.exp_number, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards)])

	# run experiment
	print ("------- execute simulation -----------\n")
	f = open("log.txt", 'w+')
	subprocess.run(['../waf', '--run', './my-handover-real-dataset --file_dir=%s --exp_number=%d --speed=%s --tx_power_add=%d --offset=%d --WindowNumber=%d --Rewards=%d' 
				%(args.file_dir, args.exp_number, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards)], stdout=f, stderr=f)
	f.close()

	# generate result statistics
	print ("-------- compare measurements and draw -----------\n")
	subprocess.run('python3 compare_measurements.py %s %d %s %d %d %d %d' 
					%(args.file_dir, args.exp_number, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards), shell=True)
	#subprocess.run('python3 compare_two_cells.py %s' %(str(exp_number)), shell=True)

	
	