#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from random import sample


if __name__ == '__main__':

	# first read all the CLI arguments and save them
	
	# assume format of rsrp filenames are in the format: rsrp_name_value.txt
	# and each .txt file has the format [time rsrp1 rsrq1 rsrp2 rsrq2]

	f = open("sinr.txt", 'r')
	contents = f.readlines()
	f.close()

	time_list = []
	sinr_list = []

	for line in contents:

		strings = line.split(' ')
		time_list.append(float(strings[0]))
		sinr_list.append(float(strings[1]))

	# actual drawing
	fig, ax = plt.subplots()
	ax.plot(time_list, sinr_list)
	#ax.axvline(x = 10.96, color='red', linestyle=':')
	ax.set(xlabel='time (s)', ylabel='SINR(dB)', title='SINR')
	ax.grid()
	fig.savefig("sinr_fig.png")

	"""
	for i in range(0, len(par_vals)):

		ax.plot(x_axis_array, rsrp_data_lists[i][1], label="cell_1,"+par_vals[i], color="blue", linestyle=line_styles[i])
		ax.plot(x_axis_array, rsrp_data_lists[i][3], label="cell_2,"+par_vals[i], color="orange", linestyle=line_styles[i])
		ax.axvline(x = rsrp_data_lists[i][5][0], color='red', linestyle=line_styles[i])

	ax.set(xlabel='time (s)', ylabel='RSRP (dBm)', title='RSRP of different '+par_name)
	ax.legend(loc='center right', ncol=1, fontsize=10)
	ax.grid()

	fig.savefig("rsrp_"+par_name+".png")

	ax.clear()

	# actual drawing
	fig, ax = plt.subplots()

	for i in range(0, len(par_vals)):

		ax.plot(x_axis_array, rsrp_data_lists[i][2], label="cell_1,"+par_vals[i], color="blue", linestyle=line_styles[i])
		ax.plot(x_axis_array, rsrp_data_lists[i][4], label="cell_2,"+par_vals[i], color="orange", linestyle=line_styles[i])
		ax.axvline(x = rsrp_data_lists[i][5][0], color='red', linestyle=line_styles[i])

	ax.set(xlabel='time (s)', ylabel='RSRQ (dB)', title='RSRQ of different '+par_name)
	ax.legend(loc='center right', ncol=1, fontsize=10)
	ax.grid()

	fig.savefig("rsrq_"+par_name+".png")

	ax.clear()
	"""