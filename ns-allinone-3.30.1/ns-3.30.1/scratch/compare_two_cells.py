#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import random
import json
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib import cm

class Measurement:
	def __init__(self, time, rsrp, rsrq):
		self.time = time
		self.rsrp = rsrp
		self.rsrq = rsrq
	def __str__(self):
		return ("<time = %f, rsrp = %f, rsrq = %f>" %(self.time, self.rsrp, self.rsrq))

"""
Based on the assumption that UE measurements performs every 0.2 second
We build the "uniform" x-axis with 0.2 x granuity
"""
if __name__ == '__main__':

	exp_number = sys.argv[1]

	meas_df = pd.read_csv('dataset/rsrp_two_cells_%s.csv' %(exp_number))

	time_list = meas_df.sim_time.to_list()
	
	rsrp_1_list = meas_df.rsrp_1.to_list()
	rsrp_2_list = meas_df.rsrp_2.to_list()
	
	#print (rsrp_1_list)
	#print (rsrp_2_list)

	rsrq_1_list = meas_df.rsrq_1.to_list()
	rsrq_2_list = meas_df.rsrq_2.to_list()

	############################################################
	fig, ax = plt.subplots()

	ax.plot(time_list, rsrp_1_list, label='Cell 1')
	ax.plot(time_list, rsrp_2_list, label='Cell 2')
	ax.grid()
	ax.legend()
	ax.set(title='Compare RSRP 1 and 2', xlabel='time(s)', ylabel='RSRP(dBm)')
	fig.savefig("figures/rsrp_two_cells_%s.png" %(exp_number))

	plt.clf()
	
	#############################################################
	fig, ax = plt.subplots()

	ax.plot(time_list, rsrq_1_list, label='Cell 1')
	ax.plot(time_list, rsrq_2_list, label='Cell 2')
	ax.grid()
	ax.legend()
	ax.set(title='Compare RSRQ 1 and 2', xlabel='time(s)', ylabel='RSRQ(dB)')
	fig.savefig("figures/rsrq_two_cells_%s.png" %(exp_number))

	plt.clf()
