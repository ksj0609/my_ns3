#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/ns2-mobility-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/command-line.h"

using namespace ns3;

/* Input: csv route file
 * Output: NS2 trace file
 *
*/

double coord_distance_3d(Vector point_1, Vector point_2){

	return std::sqrt(std::pow(point_1.x-point_2.x, 2) 
				  			 + std::pow(point_1.y-point_2.y, 2)
						   	 + std::pow(point_1.z-point_2.z, 2));
}


// read train route csv file
int main (int argc, char *argv[])
{
	//std::cout << "start" << std::endl;
  std::string file_dir;
  std::string exp_number;
  std::string speed;
  double tx_power_add;
  uint64_t offset;
  uint16_t WindowNumber;
  uint16_t Rewards;

  CommandLine cmd;
  cmd.AddValue("file_dir", "data file directory", file_dir);
  cmd.AddValue("exp_number", "No of experiment", exp_number);
  cmd.AddValue("speed", "speed of the train", speed);
  cmd.AddValue("tx_power_add", "Additional power", tx_power_add);
  cmd.AddValue("offset", "A4 offset value", offset);
  cmd.AddValue("WindowNumber", "running window size", WindowNumber);
  cmd.AddValue("Rewards", "neighbor cell rewards", Rewards);

  cmd.Parse(argc, argv);

  std::cout << "file dir = " << file_dir << std::endl;
  std::cout << "exp number = " << exp_number << std::endl;
  std::cout << "speed = " << speed << std::endl;
  std::cout << "power add = " << tx_power_add << std::endl;
  std::cout << "offset = " << (uint64_t)offset << std::endl;
  std::cout << "window number = " << (uint16_t)WindowNumber << std::endl;
  std::cout << "rewards = " << (uint16_t)Rewards << std::endl;

  std::string route_file;
  route_file = "scratch/"+file_dir+"route_"+exp_number+".csv";

  std::cout << "read route file: " << route_file << std::endl;

	std::ifstream fin;
	fin.open(route_file, std::ios::in);

	if(fin){
		std::cout << "read success" << std::endl;
	}
	else{
		std::cout << "read fail" << std::endl;
	}

	// CSV format: index, latitude, longitude, speed, datetime, mnc, earfcn, pci, cal_rsrp, total_power, rsrq, sinr              
	// read through the csv file and store useful information
	std::vector<double> latitude_list;
	std::vector<double> longitude_list;
	std::vector<double> speed_list;
	std::vector<std::string> str_time_list;

  std::vector<double> rsrp_list;
  std::vector<double> rsrq_list;

  //std::vector<double> sinr_list;
  std::vector<double> est_rsrp_list;

	std::vector<std::string> row; 
  std::string line, word;

  std::getline(fin, line); // skip the first line
 
  while (fin >> line) { 

  	// read an entire row and store it in a string variable 'line'
    // since the string contains a space (in datetime column)
    
  	row.clear();

  	std::stringstream ss(line);

    while (getline(ss, word, ',')) {
    	row.push_back(word);
    	//std::cout << line << std::endl;
    }  

		/*    
    std::cout << "-------- this line ----------" << std::endl;
    for(std::vector<std::string>::iterator it = row.begin(); it != row.end(); it++){
  		std::cout << *it << " ";
  	} 
  	std::cout << std::endl;
		
    std::cout << "lat = " << stod(row[1]) << std::endl;
    std::cout << "lon = " << stod(row[2]) << std::endl;
    std::cout << "speed = " << stod(row[3]) << std::endl;
    std::cout << "time = " << stod(row[4]) << std::endl;
    std::cout << "-------------------------------" << std::endl;
    */

  	latitude_list.push_back(stod(row[1]));
  	longitude_list.push_back(stod(row[2]));
    //std::cout << "speed = " << stod(row[3]) << std::endl;
  	speed_list.push_back(stod(row[3]));
  	
    std::cout << "speed = " << stod(row[3]) << std::endl;

    str_time_list.push_back(row[4]);


    // additional measurement data for later use
    rsrp_list.push_back(stod(row[8]));
    rsrq_list.push_back(stod(row[9]));
    //sinr_list.push_back(stod(row[12]));
    est_rsrp_list.push_back(stod(row[10]));

  }

  std::cout << "========================== start convert coordinate =========================" << std::endl;

  std::vector<Vector> ecef_coords;
  for (size_t i = 0; i < latitude_list.size(); i++){
    //std::cout << "latitude = " << latitude_list[i] << ", longitude = " << longitude_list[i];
  	ecef_coords.push_back(GeographicPositions::GeographicToCartesianCoordinates(latitude_list[i], 
  																																							longitude_list[i], 
  																																							0.0, 	// distance above the ground
  																																							GeographicPositions::WGS84));
    //Vector p = ecef_coords[i];
    //std::cout << ", < x = " << p.x << ", y = " << p.y << ", z = " << p.z << " >" << std::endl;
  }

  std::cout << "latitude list has size = " << latitude_list.size() << std::endl;

  // check the distance between point i and i+1 for all i
  for (size_t i = 0; i < ecef_coords.size()-1; i++){
  	//double distance = coord_distance_3d(ecef_coords[i], ecef_coords[i+1]);
  	//std::cout << "distance between point " << i << " and " << i+1 << " = " << distance << std::endl;
  }

  /////// using speed_list, ecef_coords, and time_list, generate NS2 3D trace file ////////////
  /*
    1. convert speed from km/h (in the dataset) to m/s (used by NS3)
    2. process timestamps, set the first row to be time 0.0, and adjust the rest
    3. first row is the initial location
    4. setup the setdest lines:
          for line i, timestamp = i'th time, speed = i'th speed, and dest is i+1'th line location  
  */

  struct std::tm t;
  std::vector<double> time_list;  // the time list with first entry to be time 0.0 
  
  /*
    If speed is original, then convert and normalize the timestamp according to the 
    actual dataset
    If speed is fixed, then calculate the timestamp based on distance and speed beween
    every two points
  */

  if (speed == "original"){

    std::vector<time_t> real_time_list;   // convert time string to time_t objects

    for (size_t i = 0; i < str_time_list.size(); i++){
      //std::cout << "time = " << time_list[i] << std::endl;
      std::istringstream ss(str_time_list[i]);
      ss >> std::get_time(&t, "%H:%M:%S");
      //std::cout << "Hour = " << t.tm_hour << ", minute = " << t.tm_min << ", sec = " << t.tm_sec << std::endl;
      time_t tt = std::mktime(&t);
      
      /*
      if (tt == -1)
        std::cout << "error happened" << std::endl;
      else
        std::cout << "no error, tt = " << tt << std::endl;
      */
      real_time_list.push_back(tt);
    }

    // normalize the timestamps, with the first to be 0.0 and rest to be the time_diff
    time_t first = real_time_list[0];
    for (size_t i = 0; i < real_time_list.size(); i++){
      time_list.push_back(difftime(real_time_list[i], first));
    }
  }
  
  else{

    double current_time = 0.0;
    time_list.push_back(current_time);

    // check the distance between point i and i+1 for all i, and calculate time based on speed
    for (size_t i = 0; i < ecef_coords.size()-1; i++){
      double distance = coord_distance_3d(ecef_coords[i], ecef_coords[i+1]);
      std::cout << "distance between point " << i << " and " << i+1 << " = " << distance << std::endl;
      double diff_time = distance / stod(speed);
      current_time += diff_time;
      time_list.push_back(current_time);
    }
  }

  /*
  for (size_t i = 0; i <= normalized_time.size(); i++){
    std::cout << "----------" << normalized_time[i] << std::endl;
  }
  */

  // trace file set initial position: $node_(0) set Y_ ..
  // trace fiel set destination: $ns_ at 0.5098790275378633 "$node_(0) setdest .. .. .. .."
  std::string movement_file;
  movement_file = "scratch/"+file_dir+"movement_"+exp_number+".txt";

  std::ofstream trace_file;
  trace_file.open(movement_file);

  // first set initial position
  trace_file << "$node_(0) set X_ " << ecef_coords[0].x << std::endl;
  trace_file << "$node_(0) set Y_ " << ecef_coords[0].y << std::endl;
  trace_file << "$node_(0) set Z_ " << ecef_coords[0].z << std::endl;

  // here read untill the second last line
  for(size_t i = 0; i < time_list.size()-2; i++){
    
    double actual_speed;
    if (speed == "original"){
      actual_speed = speed_list[i];
    }
    else{
      actual_speed = stod(speed);
    }

    //std::cout << time_list[i] << std::endl;
    trace_file << "$ns_ at " << time_list[i] << " \"$node_(0) setdest " 
                << ecef_coords[i+1].x << " " << ecef_coords[i+1].y << " " << ecef_coords[i+1].z << " "
                << actual_speed << "\"" << std::endl;
  }

  // then read the second last line
  // the reason is to skip the last std::endl, because we want to read the last line later
  size_t i = time_list.size()-2;

  double actual_speed;
  if (speed == "original"){
    actual_speed = speed_list[i];
  }
  else{
    actual_speed = stod(speed);
  }

  trace_file << "$ns_ at " << time_list[i] << " \"$node_(0) setdest " 
                << ecef_coords[i+1].x << " " << ecef_coords[i+1].y << " " << ecef_coords[i+1].z << " "
                << actual_speed << "\"";

  trace_file.close();

  ///////////////////////// generate real measurements file with the simulation time /////////////////
  std::string real_meas_file;

  real_meas_file = "scratch/"+file_dir+"real_meas_exp_"+exp_number
                           +"_speed_"+speed+"_power_"+std::to_string(int(tx_power_add))+"_offset_"+std::to_string(offset)
                           +"_window_"+std::to_string(WindowNumber)+"_rewards_"+std::to_string(Rewards)+".csv";

  trace_file.open(real_meas_file);
  trace_file << "sim_time,rsrp,rsrq,est_rsrp" << std::endl;

  for(size_t i = 0; i < time_list.size(); i++){
    trace_file << time_list[i] << "," 
               << rsrp_list[i] << "," 
               << rsrq_list[i] << "," 
               //<< sinr_list[i] << ","
               << est_rsrp_list[i] << std::endl;
  }
  trace_file.close();
  
  return 0;
}