#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from xml.dom.minidom import parse
import xml.dom.minidom

if __name__ == '__main__':

	DOMTree = xml.dom.minidom.parse("../handover_1.flowmonitor")
	collection = DOMTree.documentElement
	
	flowStats = collection.getElementsByTagName("FlowStats")

	############################### flow 1 #####################################

	flow_1_delay_hist = flowStats[0].getElementsByTagName("delayHistogram")
	bins = flow_1_delay_hist[0].getElementsByTagName("bin")
	bin_values = []
	count_values = []

	for b in bins:
		bin_values.append(float(b.getAttribute("start")))
		count_values.append(int(b.getAttribute("count")))

	# actual drawing
	fig, ax = plt.subplots()

	ax.plot(bin_values, count_values)
	ax.set(xlabel='delay (s)', ylabel='count', title='Histogram of flow 1 delay')
	ax.grid()

	fig.savefig("flow_1_delay_hist.png")

	ax.clear()

	#################################### flow 2 ####################################

	flow_2_delay_hist = flowStats[0].getElementsByTagName("delayHistogram")
	bins = flow_2_delay_hist[1].getElementsByTagName("bin")

	bin_values = []
	count_values = []

	for b in bins:
		bin_values.append(float(b.getAttribute("start")))
		count_values.append(int(b.getAttribute("count")))

	print (bin_values)
	print (count_values)

	
	# actual drawing
	fig, ax = plt.subplots()

	ax.plot(bin_values, count_values)
	ax.set(xlabel='delay (s)', ylabel='count', title='Histogram of flow 2 delay')
	ax.grid()

	fig.savefig("flow_2_delay_hist.png")

	ax.clear()
	