#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import random
import numpy as np
import pandas as pd
import statistics

import matplotlib.pyplot as plt
from matplotlib import cm

class Measurement:
	def __init__(self, time, rsrp, rsrq):
		self.time = time
		self.rsrp = rsrp
		self.rsrq = rsrq
	def __str__(self):
		return ("<time = %f, rsrp = %f, rsrq = %f>" %(self.time, self.rsrp, self.rsrq))

"""
Based on the assumption that UE measurements performs every 0.2 second
We build the "uniform" x-axis with 0.2 x granuity
"""
if __name__ == '__main__':

	my_parser = argparse.ArgumentParser()

	my_parser.add_argument('file_dir', type=str)
	my_parser.add_argument('exp_number', type=int)
	my_parser.add_argument('speed', type=str)				# speed could be 20, 30, 40, ... or 'original'
	my_parser.add_argument('tx_power_add', type=int)
	my_parser.add_argument('offset', type=int)
	my_parser.add_argument('window_number', type=int)
	my_parser.add_argument('rewards', type=int)

	args = my_parser.parse_args()

	real_meas_filename = "%sreal_meas_exp_%d_speed_%s_power_%d_offset_%d_window_%d_rewards_%d.csv" \
		%(args.file_dir, args.exp_number, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards)

	sim_meas_filename = "%ssim_meas_exp_%d_speed_%s_power_%d_offset_%d_window_%d_rewards_%d.csv" \
		%(args.file_dir, args.exp_number, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards)

	real_meas_df = pd.read_csv(real_meas_filename)
	sim_meas_df = pd.read_csv(sim_meas_filename)

	#print (real_meas_df)
	#print (sim_meas_df)

	real_meas_list = [] 	# list of real Measurement instances
	sim_meas_list = [] 		# list of simulated Measurement instances
	est_meas_list = [] 		# list of estimated Measurement instances

	for index, row in real_meas_df.iterrows():
		real_meas_list.append(Measurement(float(row['sim_time']), float(row['rsrp']), float(row['rsrq'])))
		est_meas_list.append(Measurement(float(row['sim_time']), float(row['est_rsrp']), float(row['rsrq'])))

	for index, row in sim_meas_df.iterrows():
		sim_meas_list.append(Measurement(float(row['sim_time']), float(row['rsrp']), float(row['rsrq'])))

	"""
	for m in real_meas_list:
		print (m)

	for m in sim_meas_list:
		print (m)
	"""

	real_meas_last_sec = float(real_meas_df.iloc[-1]['sim_time'])

	print ("last sec = %f" %(real_meas_last_sec))

	x_axis = np.arange(0, real_meas_last_sec, 0.2)
	#print (x_axis)

	real_rsrp = np.empty(len(x_axis))
	real_rsrp[:] = np.nan

	sim_rsrp = np.empty(len(x_axis))
	sim_rsrp[:] = np.nan

	est_rsrp = np.empty(len(x_axis))
	est_rsrp[:] = np.nan

	#real_rsrq = np.empty(len(x_axis))
	#real_rsrq[:] = np.nan

	#sim_rsrq = np.empty(len(x_axis))
	#sim_rsrq[:] = np.nan

	for i in range(0, len(x_axis)):
		
		time_value = x_axis[i]

		#print ("------------------ iteration ---------------------")
		#print ("time value = %f" %(time_value))
		
		real_mea = real_meas_list[0]
		est_mea = est_meas_list[0]

		#print ("real measure time value = %f" %(real_mea.time))

		if abs(real_mea.time-time_value) < 1e-10:
			
			#print ("same REAL time value, assign the REAL results")
			
			# it's possible that there are duplicate measurments at the same time
			measured_rsrp = []
			estimated_rsrp = []
			#measured_rsrq = []

			while len(real_meas_list) and abs(real_meas_list[0].time-time_value) < 1e-10:

				measured_rsrp.append(real_mea.rsrp)
				estimated_rsrp.append(est_mea.rsrp)
				#measured_rsrq.append(real_mea.rsrq)
				
				real_meas_list.pop(0)
				est_meas_list.pop(0)

			#real_rsrq[i] = statistics.mean(measured_rsrq)
			real_rsrp[i] = statistics.mean(measured_rsrp)
			est_rsrp[i] = statistics.mean(estimated_rsrp)

		if len(sim_meas_list):
			sim_mea = sim_meas_list[0]
			#print ("sim measure time value = %f" %(sim_mea.time))

			if abs(sim_mea.time-time_value) < 1e-10:
				#print ("same SIM time value, assign the SIM results")
				#sim_rsrq[i] = sim_mea.rsrq
				sim_rsrp[i] = sim_mea.rsrp
				sim_meas_list.pop(0)

	#print ("=================== real rsrp ================")
	#print (real_rsrp)
	
	#print ("=================== sim rsrp =================")
	#print (sim_rsrp)

	real_mask = np.isfinite(real_rsrp)
	sim_mask = np.isfinite(sim_rsrp)

	#print ("------------- real with mask -----------")
	#print (real_rsrp[real_mask])

	#print ("-------------- sim with mask -----------")
	#print (sim_rsrp[sim_mask])
	"""
	fig, ax = plt.subplots()

	ax.plot(x_axis[real_mask], real_rsrq[real_mask], linestyle='-', marker='.')
	ax.plot(x_axis[sim_mask], sim_rsrq[sim_mask], linestyle='-', marker='.', markersize=2)
	ax.set(title='Compare RSRQ', xlabel='time(s)', ylabel='RSRQ(dB)')
	ax.grid()

	fig.savefig("compare_rsrq.png")

	plt.clf()
	"""


	########################################################################################

	""" 
		Current drawing has some problem: the real_meas timestamp is "irregular",
		which means cannot find the exact X-axis value for each measurement
	"""

	"""
	fig, ax = plt.subplots()

	ax.plot(x_axis[real_mask], real_rsrp[real_mask], linestyle='-', marker='.', label='real')
	ax.plot(x_axis[real_mask], est_rsrp[real_mask], linestyle='-', marker='.', label='estimate')
	ax.plot(x_axis[sim_mask], sim_rsrp[sim_mask], linestyle='-', marker='.', label='sim')
	ax.set(title='Compare RSRP', xlabel='time(s)', ylabel='RSRP(dBm)')
	ax.grid()
	ax.legend(loc='lower right')


	fig_filename = "%scompare_rsrp_exp_%d_speed_%s_power_%d_offset_%d_window_%d_rewards_%d.png" \
		%(args.file_dir, args.exp_number, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards)

	fig.savefig(fig_filename)

	plt.clf()
	"""

	#################### check handover situation in this experiment ###################
	ho_filename = "%sho_results_speed_%s_power_%d_offset_%d_window_%d_rewards_%d.txt" \
		%(args.file_dir, args.speed, args.tx_power_add, args.offset, args.window_number, args.rewards)


	out_file = open(ho_filename, 'a')

	#rlf_happend=False
	#ho_success=False
	
	num_enbs = 0 				
	
	num_ho = 0
	num_rlf = 0

	ho_time_total = 0.0
	rlf_time_total = 0.0

	log_file = open("log.txt", 'r')
	
	ho_start_time = 0.0
	rlf_start_time = 0.0

	lines = log_file.readlines()

	first_establish = True

	ue_address = "7.0.0.2"
	host_address = "1.0.0.2"

	ue_sent = set()
	ue_received = set()
	host_sent = set()
	host_received = set()
	
	for index, line in enumerate(lines):

		if "number of eNBs" in line:
			ss = line.strip().split(' ')[-1]
			num_enbs = int(ss)
		if "HandoverStart" in line:
			num_ho += 1
			ho_start_time = float(lines[index+1].strip().split(' ')[-1])
			print ("ho start time = %f" %(ho_start_time))
		if "HandoverEndOk" in line:
			ho_end_time = float(lines[index+1].strip().split(' ')[-1])
			print ("ho end time = %f" %(ho_end_time))
			ho_time_total += (ho_end_time-ho_start_time)
		if "radio link failure" in line:
			num_rlf += 1
			rlf_start_time = float(line.strip().split(' ')[-2])
			print ("rlf start time = %f" %(rlf_start_time))
		if "ConnectionEstablished" in line:
			if first_establish:
				first_establish = False
			else:
				rlf_finish_time = float(lines[index+1].strip().split(' ')[-1])
				print ("rlf finish time = %f" %(rlf_finish_time))
				rlf_time_total += (rlf_finish_time-rlf_start_time)

		if "UdpClient:Send()" in line:
			if ue_address in line:
				# host sends to ue
				s = line.split(' ')
				i = s.index("Uid:")
				uid = int(s[i+1])
				host_sent.add(uid)
			if host_address in line:
				# ue sends to host
				s = line.split(' ')
				i = s.index("Uid:")
				uid = int(s[i+1])
				ue_sent.add(uid)
		
		if "PacketSink:HandleRead()" in line:
			#print (line)
			if ue_address in line:
				# host received from ue
				s = line.strip().split(' ')
				i = s.index("uid")
				uid = int(s[i+2])
				host_received.add(uid)
				#print ("host received uid = %d" %(uid))
			if host_address in line:
				# ue received from host
				s = line.strip().split(' ')
				i = s.index("uid")
				uid = int(s[i+2])
				ue_received.add(uid)


	num_ue_sent = len(ue_sent)
	num_host_received = len(host_received)
	num_host_sent = len(host_sent)
	num_ue_received = len(ue_received)

	log_file.close()
	out_file.write("exp %s eNBs %s num_ho %d ho_time %f num_rlf %d rlf_time %f " \
					"ue sent %d host received %d ul lost rate %f host sent %d ue received %d dl lost rate %f\n" \
					%(args.exp_number, num_enbs, num_ho, ho_time_total, num_rlf, rlf_time_total, \
						num_ue_sent, num_host_received, (num_ue_sent - num_host_received) / num_host_received, \
						num_host_sent, num_ue_received, (num_host_sent - num_ue_received) / num_ue_received))
	out_file.close()




