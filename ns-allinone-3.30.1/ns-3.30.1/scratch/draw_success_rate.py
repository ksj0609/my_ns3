#!/usr/bin/python3
import sys, ast, getopt, types
import subprocess
import argparse
import random
import numpy as np
import pandas as pd
import statistics

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib import cm

"""
Compare HO success rate
"""
def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.2}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    #rotation=45,
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

def autolabel_2(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    #rotation=45,
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

if __name__ == '__main__':

	labels = ['20', '30', '40']
	
	#l1 = [0.455856, 0.524324, 0.331532, 0.219820] 		# power=65, offset=1
	#l2 = [0.754955, 0.668468, 0.486486, 0.363964]		# power=65, offset=1, window=10
	#l3 = [0.769369, 0.690090, 0.520721, 0.403604]		# power=65, offset=1, window=5

	l1 = [0.524324, 0.331532, 0.219820]
	l2 = [0.668468, 0.486486, 0.363964]
	l3 = [0.690090, 0.520721, 0.403604]

	x = np.arange(len(labels))  # the label locations
	x = [i*2 for i in x]

	print(x)

	width = 0.6  # the width of the bars

	plt.rcParams.update({'font.size': 12})

	fig, ax = plt.subplots()
	rects1 = ax.bar([i-width for i in x], l1, width, color='tomato', label='None')
	rects2 = ax.bar(x , l2, width, color='cyan', label='Window = 10')
	rects3 = ax.bar([i+width for i in x], l3, width, color='gold', label='Window = 5')

	#ax.yaxis.set_major_formatter(mtick.PercentFormatter())

	# Add some text for labels, title and custom x-axis tick labels, etc.
	ax.set_ylabel('Success rate', fontsize=14)
	ax.set_xlabel('Speed (m/s)', fontsize=14)
	#ax.set_title('HO Success Rate w/o Improvement', fontsize=16)
	ax.set_ylim(top=0.85)
	ax.set_xticks(x)
	ax.set_xticklabels(labels)
	ax.legend()
	ax.grid(linestyle='--')

	autolabel(rects1)
	autolabel(rects2)
	autolabel(rects3)

	fig.tight_layout()
	plt.savefig('ho_rate_with_windows.png')
	plt.savefig('ho_rate_with_windows.pdf')

	plt.clf()

	######################################################################################

	labels = ['20', '30', '40']
	
	#l1 = [29, 36, 23, 19] 					# power=65, offset=1, window=5
	#l2 = [29, 35, 23, 18]					# power=65, offset=1, window=10

	l1 = [35, 23, 18]
	l2 = [36, 23, 19]

	x = np.arange(len(labels))  # the label locations
	x = [i*2 for i in x]

	print(x)

	width = 0.8  # the width of the bars

	fig, ax = plt.subplots()
	rects1 = ax.bar([i-0.5*width for i in x], l1, width, color='cyan', label='Window = 10')
	rects2 = ax.bar([i+0.5*width for i in x], l2, width, color='gold', label='Window = 5')

	#ax.yaxis.set_major_formatter(mtick.PercentFormatter())

	# Add some text for labels, title and custom x-axis tick labels, etc.
	ax.set_ylabel('Ping-pong handover count', fontsize=14)
	ax.set_xlabel('Speed (m/s)', fontsize=14)
	#ax.set_title('HO Success Rate w/o Improvement', fontsize=16)
	ax.set_ylim(top=40)
	ax.set_xticks(x)
	ax.set_xticklabels(labels)
	ax.legend()
	ax.grid(linestyle='--')

	autolabel_2(rects1)
	autolabel_2(rects2)
	#autolabel(rects3)

	fig.tight_layout()
	plt.savefig('ho_over_with_windows.png')
	plt.savefig('ho_over_with_windows.pdf')

	plt.clf()