#!/usr/bin/python3
import sys, ast, getopt, types
import random
import subprocess
import argparse
import numpy as np
import xml.dom.minidom


if __name__ == '__main__':
	
	doc = xml.dom.minidom.parse("test.xml")

	NUs = doc.getElementsByTagName("nu")

	x_points = []
	y_points = []

	for nu_tag in NUs:
		if nu_tag.getAttribute("p") == "p":
			x_points.append(float(nu_tag.getAttribute("x")))
			y_points.append(float(nu_tag.getAttribute("y")))

	for p in x_points:
		print (p)

	min_x_point = min(x_points)
	min_y_point = min(y_points)

	x_points = [i-min_x_point for i in x_points]
	y_points = [i-min_y_point for i in y_points]

	# now write the new xml file
	# just read line by line and replace the values
	# quick-and-dirty

	in_file = open("test.xml", 'r')
	contents = in_file.readlines()
	in_file.close()

	index = 0

	out_file = open("new_test.xml", 'w')

	for line in contents:
		if "p=\"p\"" in line:
			string_list = line.split(' ')
			string_list[4] = "x=\"%f\"" %(x_points[index])
			string_list[5] = "y=\"%f\"" %(y_points[index])
			new_line = ""
			for s in string_list[:-1]:
				new_line += (s + ' ')
			new_line += string_list[-1]
			out_file.write(new_line)
			index += 1
		else:
			out_file.write(line)

	out_file.close()





